package method1;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;


public class TestCode {
	
	@BeforeAll
	static void method1() {
		System.out.println("@BeforeAll the test cases executed..");
	}
	
	@BeforeEach
	void method2() {
		System.out.println("@BeforeEach test case executed..");
	}
	
	@Tag("SeatsAllocation")
	@Test
	void testMethod1() {
		System.out.println();
		System.out.println("Test for assignSeat(String section)..");
		System.out.println();
		
		AirlineReservationSystem airlinesystem = new AirlineReservationSystem();
		
		// we test whether the method will return true when"first" is passed
		System.out.println("When 'first' is passed in the parameter");
		System.out.println("------------------------------------------");
		Assertions.assertTrue(airlinesystem.assignSeat("first"));
		
		// we tests whether the method will return true when "economy" is passed
		System.out.println("When 'economy' is passed in the parameter");
		System.out.println("------------------------------------------");
		Assertions.assertTrue(airlinesystem.assignSeat("economy"));
		
		//  we tests whether the method will return false, when neither "first" nor "economy" is passed
		System.out.println("When neither 'first' nor 'economy' is passed into the parameter");
		System.out.println("----------------------------------------------------------------");
		Assertions.assertFalse(airlinesystem.assignSeat("babababa"));
		System.out.println();
		System.out.println();
		
		// we test whether the method will return false if all the seats are already occupied
		System.out.println("When all the seats have been booked");
		System.out.println("------------------------------------------");
		
		airlinesystem.makeallseatsbooked();
		Assertions.assertFalse(airlinesystem.assignSeat("economy"));
		System.out.println();
	
		
	}
	
	@Test
	void testMethod2() {
		System.out.println();
		System.out.println("Test for Seats available");
		System.out.println("----------------------------------");
	
		// test whether return true if there are seats available
		AirlineReservationSystem airlinesystem = new AirlineReservationSystem();
		Assertions.assertTrue(airlinesystem.seatsAvailable());
		System.out.println("Program returns " +airlinesystem.seatsAvailable()  +" if there are seats available");
		
		// test whether will return false if no seats are available
		airlinesystem.makeallseatsbooked();
		Assertions.assertFalse(airlinesystem.seatsAvailable());
		System.out.println("Program returns " +airlinesystem.seatsAvailable() +" if there are no seats available");
		System.out.println();
		
	}
	
	@Tag("Display")   // Staging
	@Test
	//@Disabled
	void testMethod6() {
		System.out.println();
		System.out.println("Test if display menu is correct");
		System.out.println("-----------------------------------------------");
		AirlineReservationSystem airlinesystem = new AirlineReservationSystem();
		airlinesystem.printGreeting();
		airlinesystem.printMenu();
		System.out.println();
	}
	
	
	@AfterEach
	void testMethod3() {
		System.out.println("@AfterEach test case..");
		System.out.println();
	}
	
	@AfterAll
	static void method4() {
		System.out.println("@AfterAll the test cases");
	}
}
