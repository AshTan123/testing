package method1;

import java.util.Scanner;


public class AirlineReservationSystem{

    boolean[] arrSeats = new boolean[10];
    /*public AirlineReservationSystem() {
	    for(int i = 0; i<arrSeats.length; i++) {
	    	System.out.println(arrSeats[i]);
	    }
    }*/
    
    Scanner sc = new Scanner(System.in);

    
    // Create a method that makes the array to all true (for junit testing
    public void makeallseatsbooked() {
    	for(int i = 0; i<arrSeats.length; i++) {
    		arrSeats[i] = true;
    	}
    }

    
    // SETTERS
    // assigns first empty seat in relevant section
    public boolean assignSeat(String section){
    	
    	//System.out.println(section);
    	
        if(section.equals("first")){

            if(getFreeSeats(section) > 0){

                for(int i=0; i<5; i++){

                    if(arrSeats[i] == false){

                        arrSeats[i] = true;

                        printBoardingPass(i);

                        return true;

                    }

                }

            }

        }else if(section.equals("economy")){

            if(getFreeSeats(section) > 0){

                for(int i=5; i<arrSeats.length; i++){

                    if(arrSeats[i] == false){

                        arrSeats[i] = true;

                        printBoardingPass(i);

                        return true;

                    }

                }

 

            }

        }else {
        	System.out.println("No such selection. Returning back to the main menu");
        	return false;
        }

        // seats in chosen section full

        // check if ok to assign to other section

        System.out.printf("All seats in section \"%s\" are booked.\n", section);

        System.out.printf("Would you like to be moved to section \"%s\" (y/n)? ",

                (section == "first") ? "economy" : "first");

 

        if(sc.next().charAt(0) == 'y')

            assignSeat((section == "first") ? "economy" : "first");

        else

            System.out.println("\nNext flight leaves in 3 hours.\n");

 

        return false;

    }

    // GETTERS
    // returns number of free seats in each section
    private int getFreeSeats(String section){

        int total = 0;

        if(section.equals("first")){

            // first class 1-5 (array 0-4)

            for(int i=0; i<5; i++){
            	
            	//System.out.println(arrSeats[i]);
                if(arrSeats[i] == false)
                	
                    total += 1;

            }

        }else if(section.equals("economy")){

            // economy 6-10 (array 5-9)

            for(int i=5; i<arrSeats.length; i++){

                if(arrSeats[i] == false)

                    total += 1;

            }

        }
        //System.out.println(total);
        return total;

    }

    // see whether or not all seats are booked
    public boolean seatsAvailable(){

        // if empty seat found return true
        for(boolean seat : arrSeats)

            if(seat == false)

                return true;

 

        // if none found plane is full

        return false;

    }

 
    public void printGreeting(){

        System.out.println("\nWelcome to Crap Airlines booking system.\n");

    }

    // print the menu with remaining number of seats for each section
    public void printMenu(){

        System.out.printf("1. Economy class %s\n",

                (getFreeSeats("economy") > 0) ?

                "(" + Integer.toString(getFreeSeats("economy")) + ")" : "(full)");

        System.out.printf("2. First class %s\n",

                (getFreeSeats("first") > 0 ?

                 "(" + Integer.toString(getFreeSeats("first")) + ")" : "(full)"));

        System.out.print("> ");

    }

    // prints the boarding pass
    private void printBoardingPass(int seat){

        System.out.println("\nBoarding pass for Crap Airlines.");

        System.out.printf("\nSECTION: %s\nSEAT NUMBER: %d\n\n\n",

                (seat < 5) ? "first" : "economy", seat + 1);

    }

}
