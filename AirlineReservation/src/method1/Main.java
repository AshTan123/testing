package method1;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		AirlineReservationSystem airlinesystem = new AirlineReservationSystem();
		Scanner scRef = new Scanner(System.in);
		
		
		do {
			
			airlinesystem.printGreeting();
			airlinesystem.printMenu();
			
			System.out.println();
			// Book first class or economy
			System.out.println("Which type of seats do you want to book:  (first/economy)");
			String choice = scRef.nextLine();
			airlinesystem.assignSeat(choice);
			
			if(airlinesystem.seatsAvailable() == false) {
				System.out.println("No more seats available. You may want to try and book another flight");
			}
			
			
		}while(true);
		
		

	}

}
