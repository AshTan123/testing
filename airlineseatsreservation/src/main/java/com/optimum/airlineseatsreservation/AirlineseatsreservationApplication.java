package com.optimum.airlineseatsreservation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AirlineseatsreservationApplication {

	public static void main(String[] args) {
		SpringApplication.run(AirlineseatsreservationApplication.class, args);
	}

}
