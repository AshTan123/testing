package com.optimum.airlineseatsreservation.pojo;

import org.springframework.stereotype.Component;

@Component
public class Seat {
	
	private boolean bookingStatus;
	private String bookerName;
	private String typeofSeat;
	
	public boolean isBookingStatus() {
		return bookingStatus;
	}
	public void setBookingStatus(boolean bookingStatus) {
		this.bookingStatus = bookingStatus;
	}
	public String getBookerName() {
		return bookerName;
	}
	public void setBookerName(String bookerName) {
		this.bookerName = bookerName;
	}
	public String getTypeofSeat() {
		return typeofSeat;
	}
	public void setTypeofSeat(String typeofSeat) {
		this.typeofSeat = typeofSeat;
	}
	
	@Override
	public String toString() {
		String displayresult = " bookingStatus: " +bookingStatus +"\n bookerName: " +bookerName +"\n typeofSeat: " +typeofSeat +"\n";
		return displayresult;
	}
	
	
	
	
}
