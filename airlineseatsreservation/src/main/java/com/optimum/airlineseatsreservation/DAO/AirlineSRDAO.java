package com.optimum.airlineseatsreservation.DAO;

import java.util.HashMap;

import com.optimum.airlineseatsreservation.pojo.Seat;

public interface AirlineSRDAO {
	
	// display the seats of the Airplane
	HashMap<Integer, Seat> displaySeats();
	
	// view your booking
	Seat viewMyBooking(int id);
	
	// Book seats (update)
	String makeBooking(Seat refSeat);
	
	// Delete Booking
	String deleteBooking(int id);
	
	
	
}
