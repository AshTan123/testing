package com.optimum.airlineseatsreservation.DAO;

import java.util.HashMap;

import org.springframework.stereotype.Repository;

import com.optimum.airlineseatsreservation.pojo.Seat;

@Repository
public class AirlineSRDAOImpl implements AirlineSRDAO {
	
	HashMap<Integer, Seat> refAirplaneSeats = new HashMap<Integer, Seat>();
	boolean runOnce = false;
	String result = "";
	

	public void addSeats() {
		
		Seat refSeat1 = new Seat();
		refSeat1.setBookerName("Nil");
		refSeat1.setBookingStatus(false);
		refSeat1.setTypeofSeat("first");
		
		Seat refSeat2 = new Seat();
		refSeat2.setBookerName("Nil");
		refSeat2.setBookingStatus(false);
		refSeat2.setTypeofSeat("first");
		
		Seat refSeat3 = new Seat();
		refSeat3.setBookerName("Nil");
		refSeat3.setBookingStatus(false);
		refSeat3.setTypeofSeat("first");
		
		Seat refSeat4 = new Seat();
		refSeat4.setBookerName("Nil");
		refSeat4.setBookingStatus(false);
		refSeat4.setTypeofSeat("first");
		
		Seat refSeat5 = new Seat();
		refSeat5.setBookerName("Nil");
		refSeat5.setBookingStatus(false);
		refSeat5.setTypeofSeat("economy");
		
		
		Seat refSeat6 = new Seat();
		refSeat6.setBookerName("Nil");
		refSeat6.setBookingStatus(false);
		refSeat6.setTypeofSeat("economy");
		
		Seat refSeat7 = new Seat();
		refSeat7.setBookerName("Nil");
		refSeat7.setBookingStatus(false);
		refSeat7.setTypeofSeat("economy");
		
		Seat refSeat8 = new Seat();
		refSeat8.setBookerName("Nil");
		refSeat8.setBookingStatus(false);
		refSeat8.setTypeofSeat("economy");
		
		Seat refSeat9 = new Seat();
		refSeat9.setBookerName("Nil");
		refSeat9.setBookingStatus(false);
		refSeat9.setTypeofSeat("economy");
		
		Seat refSeat10 = new Seat();
		refSeat10.setBookerName("Nil");
		refSeat10.setBookingStatus(false);
		refSeat10.setTypeofSeat("economy");
		
		refAirplaneSeats.put(101, refSeat1);
		refAirplaneSeats.put(102, refSeat2);
		refAirplaneSeats.put(103, refSeat3);
		refAirplaneSeats.put(104, refSeat4);
		refAirplaneSeats.put(105, refSeat5);
		refAirplaneSeats.put(106, refSeat6);
		refAirplaneSeats.put(107, refSeat7);
		refAirplaneSeats.put(108, refSeat8);
		refAirplaneSeats.put(109, refSeat9);
		refAirplaneSeats.put(110, refSeat10);
	
	}
	
	
	@Override
	public HashMap<Integer, Seat> displaySeats() {
		
		if(runOnce == false) {
			addSeats();
			runOnce = true;
		}
		
		return refAirplaneSeats;
	}

	@Override
	public Seat viewMyBooking(int id) {
		
		if(refAirplaneSeats.containsKey(id) == true) {
			return refAirplaneSeats.get(id);
		}
		
		return null;
	}

	@Override
	public String makeBooking(Seat refSeat) {
		if(refSeat.getTypeofSeat().equals("first")) {
			
			// set the booking
			for(Seat s: refAirplaneSeats.values()) {
				
				if(s.getTypeofSeat().equals("first") && s.isBookingStatus() == false) {
					s.setBookerName(refSeat.getBookerName());
					s.setBookingStatus(true);
					break;
					
				}

			}
			
			// get the booking
			for(Integer id: refAirplaneSeats.keySet()){
		        if(refAirplaneSeats.get(id).getBookerName().equals(refSeat.getBookerName()) && refAirplaneSeats.get(id).getTypeofSeat().equals(refSeat.getTypeofSeat())) {
		           result = "Booking Details\n----------------------------\n Booking ID: "+id +"\n Name: "+refAirplaneSeats.get(id).getBookerName() +"\n Seat Type: " +refAirplaneSeats.get(id).getTypeofSeat();
		        	return result;
		        }
		    }
			
			
		}
		
		else if(refSeat.getTypeofSeat().equals("economy")) {
				
		   // set the booking
			for(Seat s: refAirplaneSeats.values()) {
			
				if(s.getTypeofSeat().equals("economy") && s.isBookingStatus() == false) {
					s.setBookerName(refSeat.getBookerName());
					s.setBookingStatus(true);
					break;
					
				}
				
		     }
				
			  // get the booking
			for(Integer id: refAirplaneSeats.keySet()){
		        if(refAirplaneSeats.get(id).getBookerName().equals(refSeat.getBookerName()) && refAirplaneSeats.get(id).getTypeofSeat().equals(refSeat.getTypeofSeat())) {
		           result = "Booking Details\n----------------------------\n Booking ID: "+id +"\n Name: "+refAirplaneSeats.get(id).getBookerName() +"\n Seat Type: " +refAirplaneSeats.get(id).getTypeofSeat();
		        	return result;
		        }
		    }
			
		}
		
		else {
			return "No seats for " +refSeat.getTypeofSeat() +" class Available";
		}
		
		return "No seats for " +refSeat.getTypeofSeat() +" class Available";
		
	}

	@Override
	public String deleteBooking(int id) {
		if(refAirplaneSeats.containsKey(id) == true ) {
			
			if(refAirplaneSeats.get(id).isBookingStatus() == true) {
				refAirplaneSeats.get(id).setBookingStatus(false);
				refAirplaneSeats.get(id).setBookerName("Nil");
				return "Booking Successfully deleted";
			}
		}
		
		return "Booking does not exist and hence cannot be deleted";
	}

}
