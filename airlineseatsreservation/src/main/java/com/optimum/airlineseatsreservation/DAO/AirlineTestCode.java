package com.optimum.airlineseatsreservation.DAO;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.optimum.airlineseatsreservation.pojo.Seat;

public class AirlineTestCode {
	
    AirlineSRDAOImpl functions = new AirlineSRDAOImpl();
    HashMap<Integer, Seat> refHashmapseats = new HashMap<Integer, Seat>();
    
    @BeforeEach
    public void beforetestcases() {
    	Seat refSeat1 = new Seat();
		refSeat1.setBookerName("Nil");
		refSeat1.setBookingStatus(false);
		refSeat1.setTypeofSeat("first");
		
		Seat refSeat2 = new Seat();
		refSeat2.setBookerName("Nil");
		refSeat2.setBookingStatus(false);
		refSeat2.setTypeofSeat("first");
		
		Seat refSeat3 = new Seat();
		refSeat3.setBookerName("Nil");
		refSeat3.setBookingStatus(false);
		refSeat3.setTypeofSeat("first");
		
		Seat refSeat4 = new Seat();
		refSeat4.setBookerName("Nil");
		refSeat4.setBookingStatus(false);
		refSeat4.setTypeofSeat("first");
		
		Seat refSeat5 = new Seat();
		refSeat5.setBookerName("Nil");
		refSeat5.setBookingStatus(false);
		refSeat5.setTypeofSeat("economy");
		
		
		Seat refSeat6 = new Seat();
		refSeat6.setBookerName("Nil");
		refSeat6.setBookingStatus(false);
		refSeat6.setTypeofSeat("economy");
		
		Seat refSeat7 = new Seat();
		refSeat7.setBookerName("Nil");
		refSeat7.setBookingStatus(false);
		refSeat7.setTypeofSeat("economy");
		
		Seat refSeat8 = new Seat();
		refSeat8.setBookerName("Nil");
		refSeat8.setBookingStatus(false);
		refSeat8.setTypeofSeat("economy");
		
		Seat refSeat9 = new Seat();
		refSeat9.setBookerName("Nil");
		refSeat9.setBookingStatus(false);
		refSeat9.setTypeofSeat("economy");
		
		Seat refSeat10 = new Seat();
		refSeat10.setBookerName("Nil");
		refSeat10.setBookingStatus(false);
		refSeat10.setTypeofSeat("economy");
		
		refHashmapseats.put(101, refSeat1);
		refHashmapseats.put(102, refSeat2);
		refHashmapseats.put(103, refSeat3);
		refHashmapseats.put(104, refSeat4);
		refHashmapseats.put(105, refSeat5);
		refHashmapseats.put(106, refSeat6);
		refHashmapseats.put(107, refSeat7);
		refHashmapseats.put(108, refSeat8);
		refHashmapseats.put(109, refSeat9);
		refHashmapseats.put(110, refSeat10);
    }


	@Test
	public void test1() {
		assertEquals(refHashmapseats.get(101).getBookerName(), functions.displaySeats().get(101).getBookerName());
		assertEquals(refHashmapseats.get(101).getTypeofSeat(), functions.displaySeats().get(101).getTypeofSeat());
		assertEquals(refHashmapseats.get(101).isBookingStatus(), functions.displaySeats().get(101).isBookingStatus());
		
		System.out.println("Test 1");
		System.out.println("-----------------------------------------------");
		System.out.println("Details of seats in the Airplane");
		System.out.println();
		for(Seat ss: functions.refAirplaneSeats.values()) {
			System.out.println(ss.toString());
		}
	}
	
	@Test
	public void test2() {
		// initialize implementation object
		functions.addSeats();
		System.out.println("Test 2");
		System.out.println("-------------------------------------------");
		// 1) add an invalid id in the method
		assertEquals(null, functions.viewMyBooking(190));
		System.out.println("190 is not a valid booking as it returns " +functions.viewMyBooking(190) );
		System.out.println();
		
		//2) add a valid id in the method
		Seat refSeat1 = new Seat();
		refSeat1.setBookerName("Nil");
		refSeat1.setBookingStatus(false);
		refSeat1.setTypeofSeat("first");
		assertEquals(refSeat1.getBookerName(), functions.viewMyBooking(101).getBookerName());
		assertEquals(refSeat1.getTypeofSeat(), functions.viewMyBooking(101).getTypeofSeat());
		assertEquals(refSeat1.isBookingStatus(), functions.viewMyBooking(101).isBookingStatus());
		System.out.println("101 is a valid booking\n" +functions.viewMyBooking(101).toString());
	}
	
	@Test
	public void test3() {
		// initialize implementation object
		functions.addSeats();
		System.out.println("Test 3");
		System.out.println("----------------------------------------------------");
		System.out.println();
		// pass in a new refSeat with a name and first class
		Seat refSeat2 = new Seat();
		refSeat2.setBookerName("Andy");
		refSeat2.setBookingStatus(true);
		refSeat2.setTypeofSeat("first");
		String result = "Booking Details\n----------------------------\n Booking ID: 101\n Name: "+refSeat2.getBookerName() +"\n Seat Type: " +refSeat2.getTypeofSeat();
		assertEquals(result, functions.makeBooking(refSeat2));
		System.out.println(functions.makeBooking(refSeat2));
		System.out.println();
		
		
		// pass in a new refseat with a name and economy class
		Seat refSeat3 = new Seat();
		refSeat3.setBookerName("Natalie");
		refSeat3.setBookingStatus(false);
		refSeat3.setTypeofSeat("economy");
		String result2 = "Booking Details\n----------------------------\n Booking ID: 105\n Name: "+refSeat3.getBookerName() +"\n Seat Type: " +refSeat3.getTypeofSeat();
		assertEquals(result2, functions.makeBooking(refSeat3));
		System.out.println(functions.makeBooking(refSeat3));
		System.out.println();
		
		// pass in a new refseat with name and class that dont exist
		Seat refSeat4 = new Seat();
		refSeat4.setBookerName("Bobo");
		refSeat4.setBookingStatus(false);
		refSeat4.setTypeofSeat("dog");
		String result3 = "No seats for dog class Available";
		assertEquals(result3, functions.makeBooking(refSeat4));
		System.out.println(functions.makeBooking(refSeat4));
		System.out.println();
		
	}
	
	@Test
	public void test4() {
		
		System.out.println("Test 4");
		System.out.println("-----------------------------------------------");
		String result1 = "Booking Successfully deleted";
		String result2 = "Booking does not exist and hence cannot be deleted";
		// 1) add an invalid id in the method
		assertEquals(result2,  functions.deleteBooking(199));
		System.out.println(functions.deleteBooking(199));
		
		// 2) add an valid id but booking doesnt exist
		assertEquals(result2, functions.deleteBooking(103));
		System.out.println(functions.deleteBooking(103));
		
		// 3) add a valid booking 
		Seat refSeat = new Seat();
		refSeat.setBookerName("Annie");
		refSeat.setBookingStatus(true);
		refSeat.setTypeofSeat("first");
		
		// initialize implementation object
		functions.addSeats();
		functions.makeBooking(refSeat);
		String finalResult = functions.deleteBooking(101);
		assertEquals(result1, finalResult);
		System.out.println(finalResult);
		
		
	}
}
