package com.optimum.airlineseatsreservation.service;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.optimum.airlineseatsreservation.DAO.AirlineSRDAO;
import com.optimum.airlineseatsreservation.pojo.Seat;

@Service
public class AirlineSRServiceImpl implements AirlineSRService{
	@Autowired
	AirlineSRDAO refSeatDAO;

	@Override
	public HashMap<Integer, Seat> viewSeatsService() {
		return refSeatDAO.displaySeats();
	}

	@Override
	public Seat viewMyBookingService(int id) {
		return refSeatDAO.viewMyBooking(id);
	}

	@Override
	public String makeBokingService(Seat refSeat) {
		return refSeatDAO.makeBooking(refSeat);
	}

	@Override
	public String deleteBooking(int id) {
		return refSeatDAO.deleteBooking(id);
	}
	
	
}
