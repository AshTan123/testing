package com.optimum.airlineseatsreservation.service;

import java.util.HashMap;

import com.optimum.airlineseatsreservation.pojo.Seat;

public interface AirlineSRService {
	HashMap<Integer, Seat> viewSeatsService();
	Seat viewMyBookingService(int id);
	String makeBokingService(Seat refSeat);
	String deleteBooking(int id);
	
	
}
