package com.optimum.airlineseatsreservation.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.optimum.airlineseatsreservation.pojo.Seat;
import com.optimum.airlineseatsreservation.service.AirlineSRService;

@RestController
public class AirlineSRController {
	
	@Autowired
	AirlineSRService refSeatService;
	
	@GetMapping("/getseats")
	public HashMap<Integer, Seat> getSeats(){
		return refSeatService.viewSeatsService();
	}
	
	@GetMapping("/getseat/{id}")
	public Seat findBooking(@PathVariable int id) {
		return refSeatService.viewMyBookingService(id);
	}
	
	@PutMapping("/makebooking")
	public String makeBooking(@RequestBody Seat refSeat) {
		return refSeatService.makeBokingService(refSeat);
	}
	
	@PutMapping("/deletebooking/{id}")
	public String deleteBooking(@PathVariable int id) {
		return refSeatService.deleteBooking(id);
	}
}
