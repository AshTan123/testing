package quizquiz;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class QuizFunctions {
	
	public static void addquestions(HashMap<String, Quiz>refHashMap) {
		Scanner scRef = new Scanner(System.in);
		System.out.println("Enter the question: ");
		String question = scRef.nextLine();
		System.out.println("Enter Option 1: ");
		String option1 = scRef.nextLine();
		System.out.println("Enter Option 2: ");
		String option2 = scRef.nextLine();
		System.out.println("Enter Option 3: ");
		String option3 = scRef.nextLine();
		System.out.println("Enter Option 4: ");
		String option4 = scRef.nextLine();
		System.out.println("Enter the answer: ");
		String answer = scRef.nextLine();
		
		Quiz thequiz = new Quiz(question, answer, option1, option2, option3, option4);
		refHashMap.put(question, thequiz);
		System.out.println("Question successfully added!");
		
	}
	
	public static void viewquestions(HashMap<String, Quiz>refHashMap) throws IOException {
		System.out.println("Enter the Question : ");
		BufferedReader refBufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String name = refBufferedReader.readLine();
		//name = name.trim(); // remove unwanted spaces
		Quiz quizquestion = refHashMap.get(name);
		System.out.println();
		System.out.println("Details of the question");
		System.out.println("--------------------------------");
		System.out.println("Options:");
		System.out.println("a) " +quizquestion.getOption1());
		System.out.println("b) " +quizquestion.getOption2());
		System.out.println("c) " +quizquestion.getOption3());
		System.out.println("d) " +quizquestion.getOption4());
		System.out.println("Answer: " +quizquestion.getAnswer());
		System.out.println();
	}
	
	public static void viewcandidateans(HashMap<String, String>refHashMap) {
		Set<String> refSet = new HashSet<String>();
		refSet = refHashMap.keySet();
		System.out.println(refSet + "  "+refHashMap.values());
	}
	
	public static void startQuiz(HashMap<String, Quiz> hm, HashMap<String, String> anshm) {
		Scanner scRef = new Scanner(System.in);
		
		for(Quiz i: hm.values()) {
			System.out.println(i.getQuestion());
			System.out.println(i.getOption1());
			System.out.println(i.getOption2());
			System.out.println(i.getOption3());
			System.out.println(i.getOption4());
			System.out.println();
			System.out.println("Choose your answer: ");
			
			String answer = scRef.next();
			
			// put answer inside answer hashmap
			anshm.put(i.getQuestion(), answer);
			
		}
		
		System.out.println("Quiz has ended");
	}
}
