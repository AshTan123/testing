package quizquiz;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class MainQuiz {

	
	
	
	public static void main(String[] args) throws IOException {
		
		Scanner scRef = new Scanner(System.in);
		HashMap<String, Quiz> questionshashmap = new HashMap<String, Quiz>();
		
		// create a questions hashmap
		questionshashmap = QuizSet.setthequestions();
		/*Set<String> refSet = new HashSet<String>();
		refSet = questionshashmap.keySet();
		System.out.println(refSet + "  "+questionshashmap.values());*/
		
		// create a candidate's question hashmap
		HashMap<String, String> candidateanswerhashmap = new HashMap<String, String>();
		
		// create boolean
		 Boolean keepGoing = true;
		
		
		while( keepGoing = true) {
		//System.out.println(keepGoing);
		System.out.println("Enter a choice: ");
		System.out.println("1) Add more questions to the quiz.");
		System.out.println("2) Look into a particular quiz question.");
		System.out.println("3) Start Quiz.");
		System.out.println("4) Look into candidate's answers");
		System.out.println("5) Exit");
		
		String strchoice = scRef.nextLine();
		int choice = Integer.parseInt(strchoice);
		
		switch(choice){
		
		case 1:{
			QuizFunctions.addquestions(questionshashmap);
			break;
		}
		
		case 2:{
			QuizFunctions.viewquestions(questionshashmap);
			break;
		}
		
		case 3:{
			QuizFunctions.startQuiz(questionshashmap, candidateanswerhashmap);
			break;
		}
		
		case 4:{
			QuizFunctions.viewcandidateans(candidateanswerhashmap);
			break;
		}
		case 5:{
			System.exit(0);
			
		}
		
		
		}
		
	}
	}

}
