package quizquiz;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class QuizSet {
	
	
	public static HashMap<String, Quiz> setthequestions() {
		
		HashMap<String, Quiz> questions = new HashMap<String, Quiz>();
		
		Quiz refQuiz1 = new Quiz("What is Java?", "a", "It is a language", "It is an Animal", "It is the name of my teacher", "It is Amit") ;
		Quiz refQuiz2 = new Quiz("Do Astronuts only speak russian?", "b", "false", "true", "Invalid question", "Stupid question");
		Quiz refQuiz3 = new Quiz("What is 2+6", "c", "44", "26", "8", "33");
		Quiz refQuiz4 = new Quiz("What is 7+5", "d", "75", "11", "13", "12");
		
		questions.put("What is Java?", refQuiz1);
		questions.put("Do Astronuts only speak russian?", refQuiz2);
		questions.put("What is 2+6", refQuiz3);
		questions.put("What is 7+5", refQuiz4);
		
		return questions;
		
		
	}


}
