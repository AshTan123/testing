package issue7.service;

public interface OrdersService {
	void orderViewRecord();
	void orderInputInsertRecord();
	void orderInputDeleteRecord();
	void orderChoice();
	void orderInputUpdateRecord();
	void closeConnection();
	
}
