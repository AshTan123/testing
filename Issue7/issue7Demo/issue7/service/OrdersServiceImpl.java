package issue7.service;

import java.util.Scanner;

import issue7.dao.OrdersDAO;
import issue7.dao.OrdersDAOImpl;
import issue7.pojo.Order;

public class OrdersServiceImpl implements OrdersService {

	Scanner scanRef;
	OrdersDAO ordersDaoRef;
	Order orderRef;
	
	
	@Override
	public void orderViewRecord() {
		
		System.out.println("The Orders Record");
		ordersDaoRef = new OrdersDAOImpl();
		ordersDaoRef.getRecords();
		
	}

	@Override
	public void orderInputInsertRecord() {
		
		scanRef = new Scanner(System.in);
		System.out.println("Enter the new order ID: ");
		int orderid = scanRef.nextInt();
		System.out.println("Enter the customer's name: ");
		String custname = scanRef.next();
		System.out.println("Enter the item's name: ");
		String itemname = scanRef.next();
		System.out.println("Enter the price of the item: ");
		String theprice = scanRef.next();
		int price = Integer.parseInt(theprice);
		
		// set the orderid, person name, item name, and price
		orderRef = new Order();
		orderRef.setOrderID(orderid);
		orderRef.setPersonName(custname);
		orderRef.setItemName(itemname);
		orderRef.setPrice(price);
		
		// pass in the order ref into the dao ref of insert record method
		ordersDaoRef = new OrdersDAOImpl();
		ordersDaoRef.insertRecord(orderRef);
	}

	@Override
	public void orderInputDeleteRecord() {
		scanRef = new Scanner(System.in);
		System.out.println("Enter the order ID to be deleted: ");
		int orderid = scanRef.nextInt();
		System.out.println("Enter the customer's name to be deleted: ");
		String custname = scanRef.next();
		
		orderRef = new Order();
		orderRef.setOrderID(orderid);
		orderRef.setPersonName(custname);
		
		ordersDaoRef = new OrdersDAOImpl();
		ordersDaoRef.deleteRecord(orderRef);
		
		
	}

	@Override
	public void orderChoice() {
		
		scanRef = new Scanner(System.in);
		
	do{
		System.out.println("----------------------------Welcome to Ashhh's Store------------------------------");
		System.out.println("**********************************************************************************");
		System.out.println();
		System.out.println("Choose one of the choices below");
		System.out.println("1) View the Records");
		System.out.println("2) Insert a new Record");
		System.out.println("3) Delete an existing Record");
		System.out.println("4) Update an existing Record");
		System.out.println("5) Close Program");
		
		
		
		int choice = scanRef.nextInt();
		
		switch(choice){
			case 1:
				orderViewRecord();
				break;
				
			case 2:
				orderInputInsertRecord();
				break;
				
			case 3:
				orderInputDeleteRecord();
				break;
				
			case 4:
				orderInputUpdateRecord();
				break;
			case 5:
				closeConnection();
				break;
			default:
				System.out.println("Invalid choice. Going back to the main menu....");
				break;
				
				
		}
	}while(true);
		
		
		
	}

	@Override
	public void orderInputUpdateRecord() {
		scanRef = new Scanner(System.in);
		System.out.println("Enter the order ID: ");
		int orderid = scanRef.nextInt();
		System.out.println("Enter new customer's name: ");
		String custname = scanRef.next();
		System.out.println("Enter new item name: ");
		String itemname = scanRef.next();
		System.out.println("Enter new price tag: ");
		String theprice = scanRef.next();
		int price = Integer.parseInt(theprice);
		
		orderRef = new Order();
		orderRef.setOrderID(orderid);
		orderRef.setPersonName(custname);
		orderRef.setItemName(itemname);
		orderRef.setPrice(price);
		
		ordersDaoRef.updateRecord(orderRef);
		
	}

	@Override
	public void closeConnection() {
		
		ordersDaoRef = new OrdersDAOImpl();
		ordersDaoRef.closingtheConnection();
		System.exit(0);
	}

}
