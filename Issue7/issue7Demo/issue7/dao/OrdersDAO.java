package issue7.dao;

import issue7.pojo.Order;

public interface OrdersDAO {
	void getRecords();     // select * from orders table
	void insertRecord(Order orderRef);   // insert into orders table
	void deleteRecord(Order orderRef);   // delete from orders table
	void updateRecord(Order orderRef);   // update the orders table
	void closingtheConnection();
}
