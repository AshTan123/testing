package issue7.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import issue7.pojo.Order;
import utility.UtilityDatabase;



public class OrdersDAOImpl implements OrdersDAO {
	
	Connection refConnection = null;
	PreparedStatement refPreparedStatement =null;
	ResultSet rs = null;
	
	@Override
	public void getRecords() {
		
		try {
			refConnection = UtilityDatabase.getConnection();
			
			
			// mysql select query
			String sqlQuery = "select * from orders";
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			
			// execute sql statement
			ResultSet rs= refPreparedStatement.executeQuery(); 
			System.out.println("Records:");
			System.out.println();
			
			while(rs.next()){  
				
				  int id = rs.getInt("orderID");
			      String custName = rs.getString("personName");
			      String itemName = rs.getString("itemName");
			      int price = rs.getInt("price");
			 
			  
			     // print the results
			     System.out.format("%s, %s, %s, %s\n", "ID: "+id, "Name: "+custName, "Item: " +itemName, "$"+price);
			     System.out.println();
			     
				
			}  
			
			
			
			
		}catch (SQLException e) {
			System.out.println("Exception Handled while displaying record.");
		}
		
		
	}
	
	@Override
	public void insertRecord(Order orderRef) {
		
		try {
			refConnection = UtilityDatabase.getConnection();
			
			// mysql insert query
			String sqlQuery = "insert into orders(orderID, personName, itemName, price) values(?,?,?,?)";
			
			// create the mysql insert preparedstatement
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setInt(1, orderRef.getOrderID());
			refPreparedStatement.setString(2, orderRef.getPersonName());
			refPreparedStatement.setString(3, orderRef.getItemName());
			refPreparedStatement.setInt(4, orderRef.getPrice());
			
			// Approach 1
     		refPreparedStatement.execute();  //works fine
			System.out.println("Record inserted successfully");
			
			// Approach 2
//			int record = refPreparedStatement.executeUpdate();
//			if(record>0) System.out.println("new record has been successfully inserted");
			
		} catch (SQLException e) {
			System.out.println(e);
			System.out.println("Exception Handled while insert record.");
		}
		
	}

	@Override
	public void deleteRecord(Order orderRef) {
		
		try {
		refConnection = UtilityDatabase.getConnection();
		
		// mysql insert query
		String sqlQuery = "delete from orders where orderID =? AND personName =? ";
		
		// create the mysql insert preparedstatement
		refPreparedStatement = refConnection.prepareStatement(sqlQuery);
		refPreparedStatement.setInt(1, orderRef.getOrderID());
		refPreparedStatement.setString(2, orderRef.getPersonName());
		
		// Approach 1
 		refPreparedStatement.execute();  //works fine
		System.out.println("Record deleted successfully");
		
		
		} catch (SQLException e) {
			System.out.println(e);
			System.out.println("Exception Handled while deleting record.");
		}
		
		
    }

	@Override
	public void updateRecord(Order orderRef) {
		
		try {
			refConnection = UtilityDatabase.getConnection();
			
			// mysql insert query
			String sqlQuery = "UPDATE ORDERS SET personName=?, itemname=?, price=? WHERE orderID = ? ";
			
			// create the mysql insert preparedstatement
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			
			refPreparedStatement.setString(1, orderRef.getPersonName());
			refPreparedStatement.setString(2, orderRef.getItemName());
			refPreparedStatement.setInt(3, orderRef.getPrice());
			refPreparedStatement.setInt(4, orderRef.getOrderID());
			
			// Approach 1
     		refPreparedStatement.execute();  //works fine
			System.out.println("Record Updated successfully");
			
			// Approach 2
//			int record = refPreparedStatement.executeUpdate();
//			if(record>0) System.out.println("new record has been successfully inserted");
			
		} catch (SQLException e) {
			System.out.println(e);
			System.out.println("Exception Handled while Updating record.");
		}
		
		
	}

	@Override
	public void closingtheConnection() {
		try {
			refConnection = UtilityDatabase.getConnection();
			refConnection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			System.out.println("Closing Connection..");
		}
		
	}


}
