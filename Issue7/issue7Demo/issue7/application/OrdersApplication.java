package issue7.application;

import issue7.controller.OrdersController;

public class OrdersApplication {

	public static void main(String[] args) {
		OrdersController controller = new OrdersController();
		controller.startController();

	}

}
