package issue3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import issues3FunctionsAndTesting.CartFunctions;
import issues3FunctionsAndTesting.CartItem;

public class Issue3 {

	public static void main(String[] args) {
		
		 // declare important lists and scanner class
		  ArrayList<CartItem> shoppingCart = new ArrayList<CartItem>();
          Scanner scan = new Scanner(System.in);
          
          
          
          
          // HashMap Method
          //HashMap<String, CartItem> theshoppingCart = new HashMap<String, CartItem>();
          
          
          

          // declareimportant variables
          boolean keepGoing = true;
          int choice = 0;
          int index=0;
          double total = 0;
          String item;
          double price = 0.00;
          int quantity = 0;


          // while loop
          while(keepGoing)
          {

		        System.out.println("\nMenu - Managing a List");
		        System.out.println("1 Add an item to your cart");
		        System.out.println("2 Remove an item from your cart");
		        System.out.println("3 View the items in your cart");
		        System.out.println("4 Exit and add up the total");
		        System.out.println("5 Empty your cart");
		        System.out.println("6 Exit");
		        System.out.println("Select a menu option");
		        
		        String thechoice = scan.nextLine();
		        choice = Integer.parseInt(thechoice);
		
		        if (choice <1 || choice >6)
		        {
		             System.out.println("Enter a value between 1 and 6:");
		        }

		        else
		        {
                       switch (choice)
                       {
                       case 1:
                             //add an item
                             System.out.println("Enter an item:");
                             item = scan.nextLine();
                             System.out.println("Enter the price of this item: ");
                             String p = scan.nextLine();
                             price = Double.parseDouble(p);
                             System.out.println("Enter the quantity: ");
                             String q = scan.nextLine();
                             quantity = Integer.parseInt(q);
                           
                             // create a new cart item object
                             //CartItem ci = new CartItem(item, quantity, price);
                             
                             // Call the function add items
                             CartFunctions.addItems(shoppingCart, item, price, quantity);
                             break;
                             
                             // HashMap method
                             /*if(theshoppingCart.containsKey(item) == false) {
                            	 theshoppingCart.put(item, ci);
                            	 System.out.println("Item successfully added in Cart");
                            	 break;
                             }else {
                            	 System.out.println("Item already exists in Cart. Hence it is not added");
                            	 break;
                             }*/
                          

                       case 2:
                             //remove from the list
                             System.out.println("Enter an item to remove:");
                             item = scan.nextLine();
                             
                             // hashmap method to remove the item
                             /*boolean exists = theshoppingCart.containsKey(item);
                             
                             if(exists == true) {
	                             theshoppingCart.remove(item);
	                             System.out.println(item + " has been removed.");
                             }else {
                            	 System.out.println(item + " was not found in your shopping cart.");
                             }*/
                             
                             // ArrayList method. Call the remove item function
                             CartFunctions.removeItem(shoppingCart, item);
                             break;

                       case 3:

                    	     System.out.print("Items in Cart: ");
                             //view the items in your cart
                             System.out.println(CartFunctions.viewItems(shoppingCart));
                             
                             // hashmap to view items in cart
                             /*System.out.println("Items in Cart: ");
                             for(CartItem i: theshoppingCart.values()) {
                            	 System.out.print(i.getProduct() +" ");
                             }
                             System.out.println();*/
                             
                             // String format later then do
                             break;

                       case 4:
                    	     // Hashnmap method exit and add up total
                    	   	 /*for (String i : theshoppingCart.keySet()) {
                    	   		 total = total + theshoppingCart.get(i).getPrice() * theshoppingCart.get(i).getQuantity();
                    		   System.out.println("Item: " + i + "    Total Price: $" + total + "   Quantity: " +theshoppingCart.get(i).getQuantity());
                    		 }*/

                             //Exit and add up the total
                             CartFunctions.addTotalandExit(shoppingCart);
                             scan.close();
                             keepGoing = false;
                             break;

                       case 5:

                             //Empty the list
                             CartFunctions.clearCart(shoppingCart);
                             
                             // Empty HashMap
                             //theshoppingCart.clear();
                             
                             break;

                       case 6:

                             //exit
                             keepGoing = false;
                             CartFunctions.exit();
                             scan.close();
                             break;



                           }

                }

        }

}

}


