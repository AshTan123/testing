package issues3FunctionsAndTesting;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import issues3FunctionsAndTesting.CartItem;

class CartTestCode {

	@Test
	void test1() {
		System.out.println("Test 1");
		System.out.println("----------------------------------------------");
		//create empty shoppingCart Arraylist 
		ArrayList <CartItem> emptyCart = new ArrayList<CartItem>();
		
		// create a non-empty shoppingCart ArrayList 
		ArrayList <CartItem> filledCart = new ArrayList<CartItem>();
		
		// Create new Cart item
		filledCart.add(new CartItem("Fish", 20, 9.50));
		
		// assertTrue
		assertTrue(CartFunctions.addItems(emptyCart, "Fish", 9.50, 7));
		assertFalse(CartFunctions.addItems(filledCart, "Fish", 9.50, 7));
		System.out.println();
		
		//CartItem ci = new CartItem("Fish", 20, 9.50);
		//assertFalse(emptyCart.contains(ci));
	}
	
	@Test
	void test2() {
		System.out.println("Test 2");
		System.out.println("----------------------------------------------");
		// create shopping Cart where the items doesnt exist
		ArrayList <CartItem> firstCart = new ArrayList<CartItem>();
		firstCart.add(new CartItem("Berry", 20, 9.50));
		
		// create shopping cart where items want to be removed exists
		ArrayList <CartItem> secondCart = new ArrayList<CartItem>();
		secondCart.add(new CartItem("Fish", 20, 9.50));
		
		//assertTrue
		assertFalse(CartFunctions.removeItem(firstCart, "Fish"));
		assertTrue(CartFunctions.removeItem(secondCart, "Fish"));
		System.out.println();
	}
	
	@Test
	void test3() {
		
		System.out.println("Test 3");
		System.out.println("----------------------------------------------");
		// test whether String showing all the items in a shopping cart is being returned
		ArrayList <CartItem> shoppingCart = new ArrayList<CartItem>();
		shoppingCart.add(new CartItem("Berry", 20, 9.50));
		shoppingCart.add(new CartItem("Cheese", 40, 7.40));
		shoppingCart.add(new CartItem("Sandwhich", 17, 5.40));
		
		// test for empty shopping Cart
		ArrayList <CartItem> emptyCart = new ArrayList<CartItem>();
		
		// test for filled shopping cart and empty shoppping cart
		assertTrue(!CartFunctions.viewItems(shoppingCart).isEmpty());
		System.out.println("For shopping Cart, items in Cart: " +CartFunctions.viewItems(shoppingCart));
		assertFalse(!CartFunctions.viewItems(emptyCart).isEmpty());
		System.out.println("For empty Cart, items in Cart:" +CartFunctions.viewItems(emptyCart));
		System.out.println();
		
	}
	
	@Test
	void test4() {
		
		System.out.println("Test 4");
		System.out.println("----------------------------------------------");
		// Total should be 577.80
		ArrayList <CartItem> shoppingCart = new ArrayList<CartItem>();
		shoppingCart.add(new CartItem("Berry", 20, 9.50));
		shoppingCart.add(new CartItem("Cheese", 40, 7.40));
		shoppingCart.add(new CartItem("Sandwhich", 17, 5.40));
		
		assertEquals(577.80, CartFunctions.addTotalandExit(shoppingCart));
		System.out.println();
	}
	
	@Test
	void test5() {
		
		System.out.println("Test 5");
		System.out.println("----------------------------------------------");
		
		// create shopping cart
		ArrayList <CartItem> shoppingCart = new ArrayList<CartItem>();
		shoppingCart.add(new CartItem("Berry", 20, 9.50));
		shoppingCart.add(new CartItem("Cheese", 40, 7.40));
		shoppingCart.add(new CartItem("Sandwhich", 17, 5.40));
		
		//  test if shopping cart has been cleared
		assertTrue(CartFunctions.clearCart(shoppingCart).isEmpty());
	}
	
	@Test
	void test6() {
		System.out.println("Test 6");
		System.out.println("----------------------------------------------");
		CartFunctions.exit();
	}
	
	


}
