package issues3FunctionsAndTesting;

import java.util.ArrayList;


public class CartFunctions {
	
	private CartFunctions() {
		// private constructor
	}

	public static boolean addItems(ArrayList<CartItem> shoppingCart, String item, double price, int quantity) {
        
        // should you add the item?
        boolean addItem = true;
        
  
        // create a new cart item object
        CartItem ci = new CartItem(item, quantity, price);
        
        // check if the item exists in the cart
        for(int i = 0; i<shoppingCart.size(); i++) {
       	 if(ci.equals(shoppingCart.get(i)) == true){
       		 addItem = false;
       		 break;
       	 }
       	 
        }
        
        if(addItem == true) {
	       	 // add item in shopping cart
	       	 shoppingCart.add(ci);
             System.out.println("Item successfully added in Cart");
             addItem = true;

        }else {
       	 System.out.println("Item already exists in Cart. Hence it is not added");
       	 addItem = false;
        }
        
        return addItem;
        
        
        // HashMap method
        /*if(theshoppingCart.containsKey(item) == false) {
       	 theshoppingCart.put(item, ci);
       	 System.out.println("Item successfully added in Cart");
       	 break;
        }else {
       	 System.out.println("Item already exists in Cart. Hence it is not added");
       	 break;
        }*/
	}
	
	public static boolean removeItem(ArrayList<CartItem> shoppingCart, String item) {
		
		Boolean removeornot = true;
		
		 
	   	 // create a for loop to remove the item in the shopping cart
	   	 for(int i = 0; i<shoppingCart.size(); i++) {
	   		 
	   		 if(shoppingCart.get(i).getProduct().equals(item)) {
	   			 shoppingCart.remove(i);
	   			 removeornot = true;
	   			 System.out.println(item + " has been removed.");
	   			 break;
	   		 }else {
	   			 removeornot = false;
	   		 }
	   	 }
	   	 
	   	 if(removeornot == false) {
	   		 System.out.println(item + " was not found in your shopping cart.");
	   		 removeornot = false;
	   	 }
	   	 
	   	 return removeornot;
   	 
         
	}
	
	
	public static String viewItems(ArrayList<CartItem> shoppingCart) {
		String printresultout = "";
        for(int i = 0; i<shoppingCart.size(); i++) {
       	 printresultout += shoppingCart.get(i).getProduct() +" ";
        }
        return printresultout;
	}
	
	
	public static double addTotalandExit(ArrayList<CartItem> shoppingCart) {
		
		double total = 0;
		
		for (int i = 0; i<shoppingCart.size(); i++)
        {
              CartItem cici = shoppingCart.get(i);
              total = total + cici.getPrice()*cici.getQuantity();
        }

        System.out.println("Total is $"+ total);
        System.out.println("Goodbye");
        
        return total;
       
	}
	
	public static ArrayList<CartItem> clearCart(ArrayList<CartItem> shoppingCart) {
		shoppingCart.clear();
		return shoppingCart;
	}
	
	public static void exit() {
		System.out.println("Program exitted successfully. Goodbye");
	}
}
