package com.optimum.humanresource.DAO;

import java.util.HashMap;

import com.optimum.humanresource.pojo.Employee;

public interface HumanResourceDAO {
	
	// view all the employees in the company
	HashMap<Integer, Employee> viewAllEmployees();
	
	// add an employee to the company
	HashMap<Integer, Employee> addEmployee(int id, Employee refEmployee);
	
	// search for a specific employee
	Employee searchEmployee(int id);
	
	// update employee details
	HashMap<Integer, Employee> updateEmployee(int id, Employee refEmployee);
	
	// delete employee
	HashMap<Integer, Employee> deleteEmployee(int id);
	
	// Authenticate Login of employee
	String authenticateEmployee(int id, String password);
	
	
}
