package com.optimum.humanresource.DAO;

import java.util.HashMap;

import org.springframework.stereotype.Repository;

import com.optimum.humanresource.pojo.Employee;




@Repository
public class HumanResourceDAOImpl implements HumanResourceDAO {

	HashMap<Integer, Employee> refEmployeeHashMap = new HashMap<Integer, Employee>();
	public void addinsomeEmployees() {
		
		
		
		Employee employee1 = new Employee();
		employee1.setEmployeeKey(2367);
		employee1.setEmployeeName("Ashhh");
		employee1.setEmployeeDesignation("Software Engineer");
		
		Employee employee2 = new Employee();
		employee2.setEmployeeKey(7784);
		employee2.setEmployeeName("Valerie");
		employee2.setEmployeeDesignation("Human Resource Manager");
		
		Employee employee3 = new Employee();
		employee3.setEmployeeKey(3309);
		employee3.setEmployeeName("Benedict");
		employee3.setEmployeeDesignation("Accountant");
		
		Employee employee4 = new Employee();
		employee4.setEmployeeKey(9932);
		employee4.setEmployeeName("James");
		employee4.setEmployeeDesignation("Aircon Technician");
		
		refEmployeeHashMap.put(101, employee1);
		refEmployeeHashMap.put(102, employee2);
		refEmployeeHashMap.put(103, employee3);
		refEmployeeHashMap.put(104, employee4);
	}
	
	
	@Override
	public HashMap<Integer, Employee> viewAllEmployees() {
		addinsomeEmployees();
		return refEmployeeHashMap;	
	}

	@Override
	public HashMap<Integer, Employee> addEmployee(int id, Employee refEmployee) {
		
		refEmployeeHashMap.put(id, refEmployee);
		return refEmployeeHashMap;
	}

	@Override
	public Employee searchEmployee(int id) {
		
		if(refEmployeeHashMap.containsKey(id) == true) {
			return refEmployeeHashMap.get(id);
		}
		
		return null;
	}

	@Override
	public HashMap<Integer, Employee> updateEmployee(int id, Employee refEmployee) {
		
		if(refEmployeeHashMap.containsKey(id)) {
			refEmployeeHashMap.get(id).setEmployeeKey(refEmployee.getEmployeeKey());
			refEmployeeHashMap.get(id).setEmployeeName(refEmployee.getEmployeeName());
			refEmployeeHashMap.get(id).setEmployeeDesignation(refEmployee.getEmployeeDesignation());
			
			return refEmployeeHashMap;
		}
		
		return refEmployeeHashMap;
	}

	@Override
	public HashMap<Integer, Employee> deleteEmployee(int id) {
		
		if(refEmployeeHashMap.containsKey(id)) {
			refEmployeeHashMap.remove(id);
			return refEmployeeHashMap;
		}
		
		return refEmployeeHashMap;
	}

	@Override
	public String authenticateEmployee(int id, String password) {
		
		int key = Integer.parseInt(password);
		
		if(key == refEmployeeHashMap.get(id).getEmployeeKey()) {
			return "Successfully Login";
		}
		
		return "Login Unsuccessful";
		
	}

}
