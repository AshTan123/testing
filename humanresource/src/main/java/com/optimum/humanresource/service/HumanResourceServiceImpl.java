package com.optimum.humanresource.service;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.optimum.humanresource.DAO.HumanResourceDAO;
import com.optimum.humanresource.pojo.Employee;

@Service
public class HumanResourceServiceImpl implements HumanResourceService{

	@Autowired
	HumanResourceDAO refHRDAO;
	
	@Override
	public HashMap<Integer, Employee> viewAllEmployeeService() {
		return refHRDAO.viewAllEmployees();
	}

	@Override
	public HashMap<Integer, Employee> addEmployeeService(int id, Employee refEmployee) {
		return refHRDAO.addEmployee(id, refEmployee);
	}

	@Override
	public Employee searchEmployeeService(int id) {
		return refHRDAO.searchEmployee(id);
	}

	@Override
	public HashMap<Integer, Employee> updateEmployeeService(int id, Employee refEmployee) {
		return refHRDAO.updateEmployee(id, refEmployee);
	}

	@Override
	public HashMap<Integer, Employee> deleteEmployeeService(int id) {
		return refHRDAO.deleteEmployee(id);
	}

	@Override
	public String authenticationService(int id, String password) {
		return refHRDAO.authenticateEmployee(id, password);
	}

}
