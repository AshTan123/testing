package com.optimum.humanresource.service;

import java.util.HashMap;

import com.optimum.humanresource.pojo.Employee;

public interface HumanResourceService {
	
	HashMap<Integer, Employee> viewAllEmployeeService();
	HashMap<Integer, Employee> addEmployeeService(int id, Employee refEmployee);
	Employee searchEmployeeService(int id);
	HashMap<Integer, Employee> updateEmployeeService(int id, Employee refEmployee);
	HashMap<Integer, Employee> deleteEmployeeService(int id);
	String authenticationService(int id, String password);
	
}
