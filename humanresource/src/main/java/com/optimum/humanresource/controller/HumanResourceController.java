package com.optimum.humanresource.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.optimum.humanresource.pojo.Employee;
import com.optimum.humanresource.service.HumanResourceService;

@RestController
public class HumanResourceController {
	@Autowired
	HumanResourceService refHRService;
	
	@GetMapping("/getemployees")
	public HashMap<Integer, Employee> getAllEmployees(){
		return refHRService.viewAllEmployeeService();
	}
	
	@GetMapping("/getemployee/{id}")
	public Employee getAnEmployee(@PathVariable int id) {
		return refHRService.searchEmployeeService(id);
	}
	
	@PostMapping("/addemployee/{id}")
	public HashMap<Integer, Employee> addAnEmployee(@RequestBody Employee refEmployee, @PathVariable int id){
		return refHRService.addEmployeeService(id, refEmployee);
	}
	
	@PutMapping("/updateemployee/{id}")
	public HashMap<Integer, Employee> updateAnEmployee(@RequestBody Employee refEmployee, @PathVariable int id){
		return refHRService.updateEmployeeService(id, refEmployee);
	}
	
	@DeleteMapping("/deleteemployee/{id}")
	public HashMap<Integer, Employee> deleteAnEmployee(@RequestBody Employee refEmployee, @PathVariable int id){
		return refHRService.deleteEmployeeService(id);
	}
	
	// authenticate employee's key when employee enters password key
	@GetMapping("/authenticate/{id}")
	public String authentication(@RequestBody String password, @PathVariable int id) {
		return refHRService.authenticationService(id, password);
	}
	
}
