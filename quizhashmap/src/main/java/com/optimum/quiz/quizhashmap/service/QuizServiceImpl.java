package com.optimum.quiz.quizhashmap.service;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.optimum.quiz.quizhashmap.DAO.QuizDAO;
import com.optimum.quiz.quizhashmap.pojo.Question;

@Service
public class QuizServiceImpl implements QuizService {

	@Autowired
	QuizDAO refQuizDAO;
	
	@Override
	public HashMap<Integer, Question> retrieveQuestionsservice() {
		return refQuizDAO.retrieveQuestions();
	}

	@Override
	public Question findQuestionByIdservice(int id) {
		return refQuizDAO.findQuestionByID(id);
	}

	@Override
	public HashMap<Integer, Question> addQuestionservice(Question refQuestion) {
		return refQuizDAO.addQuestion(refQuestion);
	}

	@Override
	public HashMap<Integer, Question> changeQuestionservice(int id, Question refQuestion) {
		return refQuizDAO.changeQuestion(id, refQuestion);
	}

	@Override
	public HashMap<Integer, Question> deleteQuestionService(int id) {
		return refQuizDAO.deleteQuestion(id);
	}

	@Override
	public String checkAnswersservice(int id, String refAnswer) {
		return refQuizDAO.checkAnswers(id, refAnswer);
	}

}
