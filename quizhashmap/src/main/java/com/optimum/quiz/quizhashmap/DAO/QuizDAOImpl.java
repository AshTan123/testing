package com.optimum.quiz.quizhashmap.DAO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.optimum.quiz.quizhashmap.pojo.Question;

@Repository
public class QuizDAOImpl implements QuizDAO{

	HashMap<Integer, Question> refQuestionHashMap  = new HashMap<Integer, Question>();
	
	public void addQuestions() {
		
		// we add the questions into the exam paper which is the arrayList
		Question question1 = new Question();
		question1.setQuestionDetails("Do Astronauts only speak Russian?");
		question1.setQuestionID(101);
		question1.setAnswer("Yes");
		
		Question question2 = new Question();
		question2.setQuestionDetails("What is the tallest building in Singapore?");
		question2.setQuestionID(102);
		question2.setAnswer("Guoco Tower");
		
		Question question3 = new Question();
		question3.setQuestionDetails("Whose picture is on the SGD dollar notes?");
		question3.setAnswer("Enik Yusof Bin Ishak");
		question3.setQuestionID(103);
		
		
		refQuestionHashMap.put(101, question1);
		refQuestionHashMap.put(102, question2);
		refQuestionHashMap.put(103, question3);
		
	}
	

	@Override
	public HashMap<Integer, Question> retrieveQuestions() {
		addQuestions();
		return refQuestionHashMap;
		
	}

	@Override
	public HashMap<Integer, Question> addQuestion(Question refQuestion) {
		
		refQuestionHashMap.put(refQuestion.getQuestionID(), refQuestion);
		return refQuestionHashMap;
		
	}

	@Override
	public HashMap<Integer, Question> changeQuestion(int id, Question refQuestion) {
		
		
		if(refQuestionHashMap.containsKey(id) == true) {
			refQuestionHashMap.get(id).setQuestionDetails(refQuestion.getQuestionDetails());
			refQuestionHashMap.get(id).setAnswer(refQuestion.getAnswer());
			
			return refQuestionHashMap;
		}
		
		return refQuestionHashMap;
	}

	@Override
	public HashMap<Integer, Question> deleteQuestion(int id) {
		
		if(refQuestionHashMap.containsKey(id) == true) {
			refQuestionHashMap.remove(id);
			return refQuestionHashMap;
		}
		
		return refQuestionHashMap;
		
	}


	@Override
	public Question findQuestionByID(int id) {
	    
		
		
		if(refQuestionHashMap.containsKey(id) == true) {
			return refQuestionHashMap.get(id);
		}
		
		return null;
	}


	@Override
	public String checkAnswers(int id, String refAnswer) {
		
		if(refQuestionHashMap.containsKey(id) == true) {
			if((refQuestionHashMap.get(id).getAnswer()).equals(refAnswer)) {
				String result = "Your answer for Question " +id +" is correct.";
				return result;
			}else {
				String result = "Your answer for Question " +id +" is wrong.";
				return result;
			}
		}
		
		return "No such question ID.";
		
		
	}

	
}
