package com.optimum.quiz.quizhashmap.DAO;

import java.util.HashMap;
import java.util.List;


import com.optimum.quiz.quizhashmap.pojo.Question;


public interface QuizDAO {
	// get the questions
	HashMap<Integer, Question> retrieveQuestions();
	
	// search for a specific question
	Question findQuestionByID(int id);
	
	// add a question into the exam paper
	HashMap<Integer, Question> addQuestion(Question refQuestion);
	
	// Change/update the question
	HashMap<Integer, Question> changeQuestion(int id, Question refQuestion);
	
	// delete a question
	HashMap<Integer, Question> deleteQuestion(int id);
	
	// user enters answers to the quiz
	String checkAnswers(int id, String refAnswer);
	
}
