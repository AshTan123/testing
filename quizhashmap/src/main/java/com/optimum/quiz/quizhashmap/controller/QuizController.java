package com.optimum.quiz.quizhashmap.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.optimum.quiz.quizhashmap.pojo.Question;
import com.optimum.quiz.quizhashmap.service.QuizService;

@RestController
public class QuizController {

	@Autowired
	QuizService refQuizService;
	
	// get mapping for get questions
	@GetMapping("/getquestions")
	public HashMap<Integer, Question> gettheQuestionsfromQuiz(){
		return refQuizService.retrieveQuestionsservice();
	}
	
	
	// get mapping to find a specific question
	@GetMapping("/getquestions/{id}")
	public Question getaspecificQuestion(@PathVariable int id) {
		return refQuizService.findQuestionByIdservice(id);
	}
	
	// post mapping to add a question
	@PostMapping("/addquestion")   // create or add API
	public HashMap<Integer, Question> addQuestionToQuiz(@RequestBody Question refQuestion ) {
		return refQuizService.addQuestionservice(refQuestion);
	}
	
	
	//put mapping to update question
	@PutMapping("/updatequestion/{id}")
	public HashMap<Integer, Question> changequestionService(@RequestBody Question refQuestion, @PathVariable int id){
		return refQuizService.changeQuestionservice(id, refQuestion);
	}
	
	// delete mapping to delete question
	@DeleteMapping("/deletequestion/{id}")
	public HashMap<Integer, Question> deleteQuestionService(@PathVariable int id){
		return refQuizService.deleteQuestionService(id);
	}
	
	// find the answers using get mapping	
	@GetMapping("/answeraquestion/{id}")
	public String checkAnswerservice(@RequestBody String answer, @PathVariable int id) {
		return refQuizService.checkAnswersservice(id, answer);
	}
	
}
