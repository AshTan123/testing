package com.optimum.quiz.quizhashmap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuizhashmapApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuizhashmapApplication.class, args);
	}

}
