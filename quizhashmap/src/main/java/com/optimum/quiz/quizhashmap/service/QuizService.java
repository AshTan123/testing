package com.optimum.quiz.quizhashmap.service;

import java.util.HashMap;

import com.optimum.quiz.quizhashmap.pojo.Question;

public interface QuizService {
	HashMap<Integer, Question> retrieveQuestionsservice();
	Question findQuestionByIdservice(int id);
	HashMap<Integer, Question> addQuestionservice(Question refQuestion);
	HashMap<Integer, Question> changeQuestionservice(int id, Question refQuestion);
	HashMap<Integer, Question> deleteQuestionService(int id);
	String checkAnswersservice(int id, String refAnswer);
	
	
	
}
