package com.optimum.quiz.quizhashmap.pojo;

import org.springframework.stereotype.Component;

@Component
public class Question {

	    private int questionID;
	    private String questionDetails;
		private String answer;
		
		public Question() {
			
		}
		public int getQuestionID() {
			return questionID;
		}
		public void setQuestionID(int questionID) {
			this.questionID = questionID;
		}
		public String getQuestionDetails() {
			return questionDetails;
		}
		public void setQuestionDetails(String questionDetails) {
			this.questionDetails = questionDetails;
		}
		public String getAnswer() {
			return answer;
		}
		public void setAnswer(String answer) {
			this.answer = answer;
		}
		
		
		
}
