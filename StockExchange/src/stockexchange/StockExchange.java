package stockexchange;

import java.math.BigDecimal;

import java.util.HashMap;

import java.util.Map;

import java.util.concurrent.ExecutorService;

import java.util.concurrent.Executors;

import java.util.concurrent.TimeUnit;

import java.util.concurrent.locks.ReadWriteLock;

import java.util.concurrent.locks.ReentrantReadWriteLock;

 

public class StockExchange{

 

    public static void main(String[] args) throws InterruptedException {

        ExecutorService executor = Executors.newFixedThreadPool(2);

        Map<String, BigDecimal> map = new HashMap<>();

        ReadWriteLock lock = new ReentrantReadWriteLock();
        
        map.put("stock-ABC", BigDecimal.valueOf(12.35)); 
       

 
        // execute the functions
        
        while(true) {
	        executor.submit(() -> StockExchangeFunctions.writeFunction(map, lock)) ;
	        Runnable readTask = () -> StockExchangeFunctions.readFunction(map, lock) ;
	
	
	        //multiple reads
	        executor.submit(readTask);
	        executor.submit(readTask);
        
	        try {
	        	// wait half an hour before generating new s
				executor.awaitTermination(10,TimeUnit.SECONDS);
				//System.out.println("Ended");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
        
        }

       // executor.shutdown();

    }

 

}