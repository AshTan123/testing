package stockexchange;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.junit.jupiter.api.Test;

public class StockTestCode {
	ReadWriteLock lock = new ReentrantReadWriteLock();
	
	@Test
	public void testcase1() {
		
		// add an empty Map
		Map <String, BigDecimal> newmap1 = new HashMap<>();
		assertEquals("Writing .....", StockExchangeFunctions.writeFunction(newmap1, lock));
		
		// add a filled Map
		//Map <String, BigDecimal> newmap2 = new HashMap<>();
		//newmap2.put("Stock-China", )
		
	}
	
	@Test
	public void testcase2() {
		
		// dont run write method initially
		Map <String, BigDecimal> newmap3 = new HashMap<>();
		ReadWriteLock lock = new ReentrantReadWriteLock();
		assertEquals("Invalid stock", StockExchangeFunctions.readFunction(newmap3, lock));
		
		
		// run write method initially
		StockExchangeFunctions.writeFunction(newmap3, lock);
		assertEquals("Reading ....." + newmap3.get("stock-ABC"), StockExchangeFunctions.readFunction(newmap3, lock));
		
	}
}
