package stockexchange;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class StockExchangeFunctions {
	
		public static String writeFunction( Map<String, BigDecimal> map, ReadWriteLock lock) {
		{

	            lock.writeLock().lock();

	            System.out.println("Writing ....." );

	            try {

	                try {

	                    TimeUnit.SECONDS.sleep(3); //emulating update

	                } catch (Exception e) {

	                	 e.printStackTrace();

	                }
	                
	                Random r = new Random();
	                int max = 24;
	                int min = 10;
	                double x = (Math. random() * ((max - min) + 1)) + min;
	                double roundOff = Math.round(x * 100.0) / 100.0;
	                map.put("stock-ABC", BigDecimal.valueOf(roundOff));  
	                
	                
	            } finally {

	                lock.writeLock().unlock();

	            }
	            
	            return "Writing .....";

	        }
		}
		
		public static String readFunction(Map<String, BigDecimal> map, ReadWriteLock lock) {
			
			{

	            lock.readLock().lock();

	            try {

	                System.out.println("Reading ....." + map.get("stock-ABC"));

	                try {

	                    TimeUnit.SECONDS.sleep(1);

	                } catch (Exception e) {

	                    e.printStackTrace();
	                    

	                }

	            } finally {

	                lock.readLock().unlock();

	            }
	            
	            if(map.get("stock-ABC") == null) {
	            	return "Invalid stock";
	            }
	            return "Reading ....." + map.get("stock-ABC");

	        }
		}

}

