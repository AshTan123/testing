package issue3hashmap;

import java.util.HashMap;
import java.util.Scanner;

import issue3hashmapFunctionsandTesting.CartFunctionsHashmap;
import issue3hashmapFunctionsandTesting.CartItem;

public class Issue3hm {

	public static void main(String[] args) {
        
		//declare scanner class
		Scanner scan = new Scanner(System.in);
		
		
		// HashMap Method
	    HashMap<String, CartItem> theshoppingCart = new HashMap<String, CartItem>();
        

        // declareimportant variables
        boolean keepGoing = true;
        int choice = 0;
        int index=0;
        double total = 0;
        String item;
        double price = 0.00;
        int quantity = 0;


        // while loop
        while(keepGoing)
        {

		        System.out.println("\nMenu - Managing a List");
		        System.out.println("1 Add an item to your cart");
		        System.out.println("2 Remove an item from your cart");
		        System.out.println("3 View the items in your cart");
		        System.out.println("4 Exit and add up the total");
		        System.out.println("5 Empty your cart");
		        System.out.println("6 Exit");
		        System.out.println("Select a menu option");
		        
		        String thechoice = scan.nextLine();
		        choice = Integer.parseInt(thechoice);
		
		        if (choice <1 || choice >6)
		        {
		             System.out.println("Enter a value between 1 and 6:");
		        }

		        else
		        {
                     switch (choice)
                     {
                     case 1:
                           //add an item
                           System.out.println("Enter an item:");
                           item = scan.nextLine();
                           System.out.println("Enter the price of this item: ");
                           String p = scan.nextLine();
                           price = Double.parseDouble(p);
                           System.out.println("Enter the quantity: ");
                           String q = scan.nextLine();
                           quantity = Integer.parseInt(q);
                           CartItem refCartItem = new CartItem(item, quantity, price);
                           CartFunctionsHashmap.addItems(theshoppingCart, refCartItem);
                           break;
                        

                     case 2:
                           //remove from the list
                           System.out.println("Enter an item to remove:");
                           item = scan.nextLine();
                           CartFunctionsHashmap.removeItems(theshoppingCart, item);
                           break;
                           

                     case 3:
                    	   // view the shopping cart
                           CartFunctionsHashmap.viewItems(theshoppingCart);
                           break;

                     case 4:
                  	      
                    	   CartFunctionsHashmap.addTotal(theshoppingCart);
                    	   keepGoing = CartFunctionsHashmap.exitLoop(keepGoing);
                           break;

                     case 5:
                    	   CartFunctionsHashmap.emptytheCart(theshoppingCart);
                           break;

                     case 6:
                           //exit
                    	   keepGoing = CartFunctionsHashmap.exitLoop(keepGoing);
                           scan.close();
                           break;

                         }

              }

      }

	}

}
