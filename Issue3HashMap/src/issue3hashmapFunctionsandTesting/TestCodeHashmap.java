package issue3hashmapFunctionsandTesting;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.HashMap;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestCodeHashmap {
	
	// create shopping cart references
	HashMap<String, CartItem> refShoppingCart;
	HashMap<String, CartItem> refShoppingCart2;
	HashMap<String, CartItem> refShoppingCart3;
	HashMap<String, CartItem> refShoppingCart4;
	HashMap<String, CartItem> refShoppingCart5;
	HashMap<String, CartItem> refShoppingCart6;
	HashMap<String, CartItem> refShoppingCart7;
	
	// create boolean reference for program termination
	boolean keepGoing = false;
	
	
	// Test if can add Item in an empty cart
	@BeforeEach
	public void beforecase1() {
		// empty shopping cart
		refShoppingCart = new HashMap<String, CartItem>();
		
		// shopping cart with same item
		refShoppingCart2 = new HashMap<String, CartItem>();
		
		// empty shopping cart
		refShoppingCart3 = new HashMap<String, CartItem>();
		
		// shopping cart that contains item to be removed
		refShoppingCart4 = new HashMap<String, CartItem>();
		
		// shopping cart to be viewed 
		refShoppingCart5 = new HashMap<String, CartItem>();
		
		// shopping cart to tally up total
		refShoppingCart6 = new HashMap<String, CartItem>();
		
		// shopping cart to be cleared
		refShoppingCart7 = new HashMap<String, CartItem>();
		
		
		//Test on empty shopping cart
		//CartFunctionsHashmap.addItems(refShoppingCart, new CartItem("Beer", 3, 9.70));
		
		// Test on shopping cart that contains same item
		//CartFunctionsHashmap.addItems(refShoppingCart2, new CartItem("Beer", 3, 9.70));
		
	}
	
	@Test
	public void testCase1() {
		
		assertEquals("Item successfully added in Cart", CartFunctionsHashmap.addItems(refShoppingCart, new CartItem("Beer", 3, 9.70)));
		
	}
	
	@AfterEach
	public void aftercase1() {
		System.out.println("Ended Test");
	}
	
	
	// Test add item on cart with same value as the item that is going to be added
	@BeforeEach
	public void beforecase2() {
		
		
		CartItem refCartItem = new CartItem("Beer", 3, 9.50);
		refShoppingCart2.put("Beer", refCartItem);
		
		//Test on empty shopping cart
		//CartFunctionsHashmap.addItems(refShoppingCart, new CartItem("Beer", 3, 9.70));
		
		// Test on shopping cart that contains same item
		//CartFunctionsHashmap.addItems(refShoppingCart2, new CartItem("Beer", 3, 9.70));
		
	}
	
	@Test
	public void testCase2() {
		
		assertEquals("Item already exists in Cart. Hence it is not added", CartFunctionsHashmap.addItems(refShoppingCart2, new CartItem("Beer", 3, 9.70)));
		
	}
	
	@AfterEach
	public void aftercase2() {
		System.out.println("Ended Test");
	}
	
	// test removeItem() on empty shopping cart
	@BeforeEach
	public void beforecase3() {
		
		// empty shopping Cart
		refShoppingCart3 = new HashMap<String, CartItem>();	
	}
	
	@Test
	public void testCase3() {
		
		assertEquals("BlueTable was not found in your shopping cart.", CartFunctionsHashmap.removeItems(refShoppingCart3, "BlueTable"));
		
	}
	
	@AfterEach
	public void aftercase3() {
		System.out.println("Ended Test");
	}
	
	// test removeItem() on shopping cart that contains the item that needs to be removed
	@BeforeEach
	public void beforecase4() {
		
		refShoppingCart4 = new HashMap<String, CartItem>();	
		
		// filled shopping Cart
		refShoppingCart4= new HashMap<String, CartItem>();	
		refShoppingCart4.put("BlueTable", new CartItem("BlueTable", 3, 19.20));
		refShoppingCart4.put("RedChair", new CartItem("RedChair", 5, 14.60));
	}
	
	@Test
	public void testCase4() {
		
		assertEquals("BlueTable has been removed.", CartFunctionsHashmap.removeItems(refShoppingCart4, "BlueTable"));
		
	}
		
	@AfterEach
	public void aftercase4() {
		System.out.println("Ended Test");
	}
	
	
	@BeforeEach
	public void beforecase5() {
		
		refShoppingCart5 = new HashMap<String, CartItem>();	
		
		// filled shopping Cart
		refShoppingCart5= new HashMap<String, CartItem>();	
		refShoppingCart5.put("BlueTable", new CartItem("BlueTable", 3, 19.20));
		refShoppingCart5.put("RedChair", new CartItem("RedChair", 5, 14.60));
		refShoppingCart5.put("GreenSofa", new CartItem("GreenSofa", 2, 50.90));
		refShoppingCart5.put("YellowStool", new CartItem("YellowStool", 4, 10.90));
	}
	
	@Test
	public void testCase5() {
		
		assertEquals("Items in Cart: YellowStool BlueTable RedChair GreenSofa ", CartFunctionsHashmap.viewItems(refShoppingCart5));
		
	}
		
	@AfterEach
	public void aftercase5() {
		System.out.println("Ended Test");
	}
	
	
	// check if total is correct
	//check if the program can end successfully
	@BeforeEach
	public void beforecase6() {
		
		refShoppingCart6 = new HashMap<String, CartItem>();	
		
		// filled shopping Cart
		refShoppingCart6= new HashMap<String, CartItem>();	
		refShoppingCart6.put("BlueTable", new CartItem("BlueTable", 3, 19.20));
		refShoppingCart6.put("RedChair", new CartItem("RedChair", 5, 14.60));
		refShoppingCart6.put("GreenSofa", new CartItem("GreenSofa", 2, 50.90));
		refShoppingCart6.put("YellowStool", new CartItem("YellowStool", 4, 10.90));
		
		
	}
	
	@Test
	public void testCase6() {
		
		assertEquals(276.00, CartFunctionsHashmap.addTotal(refShoppingCart6));
		assertFalse(CartFunctionsHashmap.exitLoop(false));
		
	}
		
	@AfterEach
	public void aftercase6() {
		System.out.println("Ended Test");
	}
	
	@BeforeEach
	public void beforecase7() {
		
		refShoppingCart7 = new HashMap<String, CartItem>();	
		
		// filled shopping Cart
		refShoppingCart7= new HashMap<String, CartItem>();	
		refShoppingCart7.put("BlueTable", new CartItem("BlueTable", 3, 19.20));
		refShoppingCart7.put("RedChair", new CartItem("RedChair", 5, 14.60));
		refShoppingCart7.put("GreenSofa", new CartItem("GreenSofa", 2, 50.90));
		refShoppingCart7.put("YellowStool", new CartItem("YellowStool", 4, 10.90));
		
		
	}
	
	@Test
	public void testCase7() {
		
		HashMap<String, CartItem> refShoppingCart8 = new HashMap<String, CartItem>();
		assertEquals(refShoppingCart8, CartFunctionsHashmap.emptytheCart(refShoppingCart7));
		
		
	}
		
	@AfterEach
	public void aftercase7() {
		System.out.println("Ended Test");
	}
	
	// test if can exit program successfully
	@BeforeEach
	public void beforecase8() {
		//System.out.println("Test if program can exit successfully");
	}
	
	@Test
	public void testCase8() {
		
		assertFalse(CartFunctionsHashmap.exitLoop(keepGoing));
		
	}
		
	@AfterEach
	public void aftercase8() {
		System.out.println("Ended Test");
	}
	
	
	
}
