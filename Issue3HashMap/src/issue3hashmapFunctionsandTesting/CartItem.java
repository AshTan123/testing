package issue3hashmapFunctionsandTesting;

public class CartItem{
	
		private String product;
		private int quantity;
		private double price;

		private CartItem() {
	
		}

		//constructor with parameters

		public CartItem(String inProduct, int inQuant, double inPrice)
		{
		   product = new String(inProduct);
		   quantity = inQuant;
		   price = inPrice;
        }

		public String getProduct()
		{

			return product;

		}

		public double getPrice()
		{

			return price;

		}

		public int getQuantity()
		{

			return quantity;

		}


		//getter setter public methods for each instance data
		public boolean equals(CartItem item)
		{

			//write the code for the equals method

			//return true;

			boolean result = false;

			if (this.product.equalsIgnoreCase(item.getProduct()) && this.price == item.getPrice())

                 result = true;

			/* else

                 result = false;*/


			return result;

		}
		
}