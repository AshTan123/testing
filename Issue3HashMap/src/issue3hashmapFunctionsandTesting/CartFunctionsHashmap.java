package issue3hashmapFunctionsandTesting;

import java.util.HashMap;


public class CartFunctionsHashmap {
	
	static double total;
	static double priceofItem;
	
	private CartFunctionsHashmap() {
		
	}

	public static String addItems(HashMap<String, CartItem> refShoppingCart, CartItem cartItem){
        // String
		String output = "";
        // HashMap method
        if(refShoppingCart.containsKey(cartItem.getProduct()) == false) {
       	 	refShoppingCart.put(cartItem.getProduct(), cartItem);
       	 	System.out.println("Item successfully added in Cart");
       	 	output = "Item successfully added in Cart";
       	 
        }else {
       	 	System.out.println("Item already exists in Cart. Hence it is not added");
       	 	output = "Item already exists in Cart. Hence it is not added";
        }
        
        return output;
        
	}
	
	public static String removeItems(HashMap<String, CartItem> theshoppingCart, String item){
		
		// String output
		String output = "";
		
		 // hashmap method to remove the item
        boolean exists = theshoppingCart.containsKey(item);
        
        if(exists == true) {
              theshoppingCart.remove(item);
              System.out.println(item + " has been removed.");
              output = item + " has been removed.";
        }else {
       	 System.out.println(item + " was not found in your shopping cart.");
       	 output = item + " was not found in your shopping cart.";
        }
        
        return output;
	}
	
	public static String viewItems(HashMap<String, CartItem> theshoppingCart) {
		
			// initialize string builder
			StringBuilder sb = new StringBuilder();
			
			// hashmap to view items in cart
	        System.out.println("Items in Cart: ");
	        sb.append("Items in Cart: ");
	        for(CartItem i: theshoppingCart.values()) {
	       	 	System.out.print(i.getProduct() +" ");
	       	 	sb.append(i.getProduct() +" ");
	        }
	        System.out.println();
	        
	        // convert stringbuilder to string
	        String result = sb.toString();
	        return result;
	}
	
	public static double addTotal(HashMap<String, CartItem> theshoppingCart) {
		
		// Hashmap method exit and add up total
 	   	 for (String i : theshoppingCart.keySet()) {
 	   		 priceofItem = theshoppingCart.get(i).getPrice() * theshoppingCart.get(i).getQuantity();
 	   		 total = total + theshoppingCart.get(i).getPrice() * theshoppingCart.get(i).getQuantity();
 		   System.out.println("Item: " + i + "    Total Price: $" + priceofItem + "   Quantity: " +theshoppingCart.get(i).getQuantity());
 		 }
 	   	 
 	   	 System.out.println("The total price is " +total);
 	   	 
 	   	 return total;
 	   	 
 	   	 
	}
	
	public static boolean exitLoop(boolean refKeepGoing) {
		refKeepGoing = false;
		System.out.println("Program exitted successfully. Goodbye");
		return refKeepGoing;
	}
	
	public static HashMap<String, CartItem> emptytheCart(HashMap<String, CartItem> refShoppingCart) {
		 // Empty HashMap
        refShoppingCart.clear();
        System.out.println("The shopping cart has been cleared.");
        return refShoppingCart;
        
	}

	
	
	
	
}
