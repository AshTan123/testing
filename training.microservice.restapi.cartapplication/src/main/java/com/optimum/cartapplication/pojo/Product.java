package com.optimum.cartapplication.pojo;

import org.springframework.stereotype.Component;

@Component
public class Product {
	
	private int productID;
	private String productName;
	private Double productPrice;
	
	public Product() {
		
	}
	
	public Product(int l, String name, double price) {
		productID = l;
		productName = name;
		productPrice = price;
	}
	
	public int getProductID() {
		return productID;
	}
	public void setProductID(int i) {
		this.productID = i;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Double getProductprice() {
		return productPrice;
	}
	public void setProductprice(Double productPrice) {
		this.productPrice = productPrice;
	}
	
	@Override
	public String toString() {
		return productID + " " +productName + " " +productPrice;
	}
	
}
