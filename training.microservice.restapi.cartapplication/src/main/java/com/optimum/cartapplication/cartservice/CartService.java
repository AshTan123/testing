package com.optimum.cartapplication.cartservice;

import java.util.List;

import com.optimum.cartapplication.pojo.Product;

public interface CartService {
	
	Product addproductService(Product refProduct);
	List<Product> getallProductsService();
	Product getProductByIDService(int id);
	Product updateProductService(int id, Product refProduct);
	List<Product> deleteProductService(int id);
	
	
	
}
