package com.optimum.cartapplication.cartservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.optimum.cartapplication.dao.CartDAO;
import com.optimum.cartapplication.dao.CartDAOImpl;
import com.optimum.cartapplication.pojo.Product;

@Service
public class CartServiceImpl implements CartService {

	@Autowired
	CartDAO refCartDAO;
	
	@Override
	public Product addproductService(Product refProduct) {
		
		return refCartDAO.addProductDAO(refProduct);
		
	}

	@Override
	public List<Product> getallProductsService() {
		return refCartDAO.getAllProducts();
	}

	@Override
	public Product getProductByIDService(int id) {
		return refCartDAO.getProductByID(id);
	}

	@Override
	public Product updateProductService(int id, Product refProduct) {
		return refCartDAO.updateproduct(id, refProduct);
		
	}

	@Override
	public List<Product> deleteProductService(int id) {
		return refCartDAO.deleteproduct(id);
			
	}

}
