package com.optimum.cartapplication.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.optimum.cartapplication.pojo.Product;


// Spring Data JPA
@Repository
public class CartDAOImpl implements CartDAO {

	ArrayList<Product> refArrayList;
	
	public void addProduct() {
		/*refArrayList.add(new Product(101L, "Airpod", 100.50));
		refArrayList.add(new Product(102L, "Speaker", 200.50));
		refArrayList.add(new Product(103L, "Mobile", 800.50));*/
		Product refProduct = new Product();
		refProduct.setProductID(101);
		refProduct.setProductName("Airpods");
		refProduct.setProductprice(120.50);
		
		Product refProduct2 = new Product();
		refProduct2.setProductID(102);
		refProduct2.setProductName("Shaver");
		refProduct2.setProductprice(77.50);
		
		refArrayList = new ArrayList();
		refArrayList.add(refProduct);
		refArrayList.add(refProduct2);
	}
	
	@Override
	public Product addProductDAO(Product refproduct) {
		
		System.out.println("Successfully added to cart");
		System.out.println(refproduct.toString());
		return refproduct;
		
	} // end of add product

	@Override
	public List<Product> getAllProducts() {
		return refArrayList;
	}

	@Override
	public Product getProductByID(int id) {
		addProduct();
		for(Product refproduct: refArrayList) {
			if(refproduct.getProductID() == id) {
				return refproduct;
			}
		}
		
		return null;
	}

	@Override
	public Product updateproduct(int id, Product refProduct) {
		addProduct();
		for(Product refproductnew : refArrayList) {
			if(refproductnew.getProductID() == id) {
				
				refproductnew.setProductName(refProduct.getProductName());
				refproductnew.setProductprice(refProduct.getProductprice());
				System.out.println(refArrayList.toString());
				System.out.println("Product updated successfully");
				return refproductnew;
			}
				
		}
			
		return null;
			
		
	 }
		
	

	@Override
	public List<Product> deleteproduct(int id) {
		addProduct();
		
		for(Product refproducttemp : refArrayList) {
			if(refproducttemp.getProductID() == id) {
				refArrayList.remove(refproducttemp);
				System.out.println(refArrayList);
				System.out.println("Product deleted successfully.");
				return refArrayList;
			}
		}
		
		return refArrayList;
		
		
	}

}
