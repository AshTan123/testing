package com.optimum.cartapplication.dao;

import java.util.List;

import com.optimum.cartapplication.pojo.Product;


public interface CartDAO {
	
	Product addProductDAO(Product refproduct);
	List<Product> getAllProducts();
	Product getProductByID(int id);
	Product updateproduct(int id, Product refProduct);
	List<Product> deleteproduct(int id);
}
