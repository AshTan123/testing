package com.optimum.cartapplication.cartcontroller;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.optimum.cartapplication.cartservice.CartService;
import com.optimum.cartapplication.cartservice.CartServiceImpl;
import com.optimum.cartapplication.pojo.Product;

@RestController
public class CartController {
	
	@Autowired
	CartService refCartService;
	
	ArrayList refArrayList;
	
	public void addProduct() {
		/*refArrayList.add(new Product(101L, "Airpod", 100.50));
		refArrayList.add(new Product(102L, "Speaker", 200.50));
		refArrayList.add(new Product(103L, "Mobile", 800.50));*/
		Product refProduct = new Product();
		refProduct.setProductID(101);
		refProduct.setProductName("Airpods");
		refProduct.setProductprice(120.50);
		
		refArrayList = new ArrayList();
		refArrayList.add(refProduct);
	}
	
	// add products to cart
	@PostMapping("/products")   // create or add API
	public Product addProductTocart(@RequestBody Product refProduct) {
		return refCartService.addproductService(refProduct);
	}
	
	// get products of cart
	@GetMapping("/getallproducts")     // fetch or retrieve all records
	public List<Product> getAllProductsFromCart(){
		addProduct();
		return refArrayList;
	}
	
	// get specific product from cart
	@GetMapping("/product/{id}")
	public Product getProductByID(@RequestBody Product refProduct, @PathVariable int id) {
		return refCartService.getProductByIDService(id);
		
	}
	
	// update the products
	@PutMapping("/products/{id}")
	public Product updateProduct(@RequestBody Product refProduct, @PathVariable int id) {
		return refCartService.updateProductService(id, refProduct);
	}
	
	// delete products
	@DeleteMapping("/products/{id}")
	public List<Product> deleteProductService(@PathVariable int id) {
		return refCartService.deleteProductService(id);
	}
	
	
}
