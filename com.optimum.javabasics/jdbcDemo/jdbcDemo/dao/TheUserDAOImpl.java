package jdbcDemo.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jdbcDemo.pojo.User;
import Utility.DatabaseUtility;

public class TheUserDAOImpl implements TheUserDAO {
	Connection refConnection = null;
	PreparedStatement refPreparedStatement =null;
	
	@Override
	public void getUserRecord() {
		
		// refConnection ==> this connection reference we can use for our SQL queries
		
	}

	@Override
	public void insertRecord(User refUser) {
				
		try {
			refConnection = DatabaseUtility.getConnection();
			
			// mysql insert query
			String sqlQuery = "insert into user(user_id,user_password) values(?,?)";
			
			// create the mysql insert preparedstatement
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setInt(1, refUser.getUserID());
			refPreparedStatement.setString(2, refUser.getUserPassword());
			
			// Approach 1
//			refPreparedStatement.execute();  works fine
			
			// Approach 2
//			int record = refPreparedStatement.executeUpdate();
//			if(record>0) System.out.println("new record has been successfully inserted");
			
		} catch (SQLException e) {
			System.out.println("Exception Handled while insert record.");
		}
		
		finally {
			try {
				refConnection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			finally {
				System.out.println("Closing Connection..");
			}
		}
		
	} // insertRecord

	@Override
	public void deleteRecord() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateRecord() {
		// TODO Auto-generated method stub
		
	}
}
