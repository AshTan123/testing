package jdbcDemo.dao;

import jdbcDemo.pojo.User;

public interface TheUserDAO {
	
	void getUserRecord();
	void insertRecord(User refUser);
	void deleteRecord();
	void updateRecord();

	// void userLogin();
	
}