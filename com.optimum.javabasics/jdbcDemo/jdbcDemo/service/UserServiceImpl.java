package jdbcDemo.service;

import java.util.Scanner;

import jdbcDemo.dao.TheUserDAO;
import jdbcDemo.dao.TheUserDAOImpl;
import jdbcDemo.pojo.User;


public class UserServiceImpl implements UserService{
	TheUserDAO refUserDAO;
	Scanner scannerRef;
	User refUser;
	
	public void userInputInsertRecord() {
		scannerRef = new Scanner(System.in);
		
		System.out.println("Enter User ID : ");
		int userLoginID = scannerRef.nextInt();
		
		System.out.println("Enter User Password : ");
		String userPassword = scannerRef.next();
		
		refUser = new User();
		refUser.setUserID(userLoginID);
		refUser.setUserPassword(userPassword);
		
		refUserDAO = new TheUserDAOImpl();
		refUserDAO.insertRecord(refUser);
	
	} // end of userInputInsertRecord
	
	public void userChoice() {
		
		System.out.println("Enter Choice");
		scannerRef = new Scanner(System.in);
		int choice = scannerRef.nextInt();
		switch (choice) {
		case 1:
			userInputInsertRecord();
			break;

		default:
			System.out.println("Option not found..");
			break;
		} // end of userChoice
	
	} // end of userChoice
}
