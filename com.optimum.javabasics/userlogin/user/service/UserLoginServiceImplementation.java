package user.service;

import java.util.Scanner;

import user.dao.UserLoginDAOAuthenticationImplementation;
import user.dao.UserLoginDAOInterface;
import user.pojo.User;

public class UserLoginServiceImplementation implements UserLoginServiceInterface{

	UserLoginDAOInterface refUserLoginDAOInterface = null;
	User refUser;
	
	@Override
	public void callUserDAOImplementation() {
		
		refUser = new User();
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter user name : ");
		String name = sc.next();
		
		refUser.setUserLoginID(name);			// it will call and set the value to User POJO class file
		
		System.out.println("Enter user password : ");
		String password = sc.next();
		refUser.setPassword(password);         // it will call and set the value to User POJO class file
		
		refUserLoginDAOInterface = new UserLoginDAOAuthenticationImplementation();
		
		if(refUserLoginDAOInterface.userLoginAuthentication(refUser) == true) {
			System.out.println("DashBoard Displayed");
		}else {
			System.out.println("No DashBoard displayed");
		}
					// if this condition return true, then call user dash board
					// else redirect to user login home page
		
	}
	
	

}
