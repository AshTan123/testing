package user.dao;

import user.pojo.User;

public interface UserLoginDAOInterface {

	boolean userLoginAuthentication(User refUser);
	
}
