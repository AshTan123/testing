package customer.service;

import java.util.Scanner;

import customer.dao.CustomerAuthentication;
import customer.dao.CustomerAuthenticationInterface;
import customer.pojo.Customer;

public class CustomerService implements CustomerServiceInterface{
	
	CustomerAuthenticationInterface refCA = null;

	@Override
	public void Service() {
		
		
		Customer refCustomer = new Customer();
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the username: ");
		String name = sc.next();
		refCustomer.setUsername(name);
		System.out.println("Enter your pin: ");
		String pin = sc.next();
		refCustomer.setPin(pin);
		
		refCA = new CustomerAuthentication();
		
		if(refCA.Authentication(refCustomer) == true) {
			System.out.println("Valid Customer");
		}else {
			System.out.println("Invalid Customer");
		}
		
		
		
	}
	
	

}
