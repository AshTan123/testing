package jdbc.dao;

import java.sql.Connection;

import utility.DBUtility;

public class UserDAOImpl implements UserDAO {

	Connection refConnection = DBUtility.getConnection();
	
	void checkConnectionStatus() {
		System.out.println("Connection Success..");
	}
	@Override
	public void getuserRecord() {
		
		// refConnection ==> this connection reference we can use for our SQL quires.
		
	}

	@Override
	public void insertRecord() {
		// refConnection ==> this connection reference we can use for our SQL quires.
		
	}

}
