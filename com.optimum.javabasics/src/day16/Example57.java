package day16;

// use case: suppose we have multiple thread running in our application and we need to execute(run) any perticular
// thread on a high priority then how to solve it
// thread is not an abstract class

class ThreadDemo extends Thread{
	@Override
	public void run() {
		for(int i = 0; i<=3; i++) {
			try {
				Thread.sleep(5000); // 5 seconds
			}
			catch(Exception e) {
				System.out.println("Exception Handled..");
			}
			
			System.out.println(i);
			System.out.println(currentThread().getName()); // under main() thread get the thread(sub process)
		}
	}
	
}

public class Example57 {

	public static void main(String[] args) {  // main thread
		
		ThreadDemo refThreadDemo = new ThreadDemo();
		
		Thread refThread1 = new Thread(refThreadDemo);
		Thread refThread2 = new Thread(refThreadDemo);
		Thread refThread3 = new Thread(refThreadDemo);
		Thread refThread4 = new Thread(refThreadDemo);
		Thread refThread5 = new Thread(refThreadDemo);
		
		refThread1.setName("Thread-1");
		refThread2.setName("Thread-2");
		refThread3.setName("Thread-3");
		refThread4.setName("Thread-4");
		refThread5.setName("Thread-5");
		
		refThread1.start();
		refThread2.start();
		refThread3.start();
		/*try {
			refThread3.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		refThread4.start();
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		refThread5.start();
		

	}

}
