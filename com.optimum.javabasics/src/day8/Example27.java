package day8;

// Type Casting

// Primitive Data Types (8)

// byte short char int long double float boolean

// Wrapper Class (Since Java Collection Framework doesn't accept any primitive data types, it only accepts classes and Wrapper classes)

// Byte   Short   Character  Integer  Long  Float  Double  Boolean

class CastingDemo{
	
	void getData1() {
		
		int number1 = 100;
		long number2 = number1;   // implicit casting done by JVM
		
		float number3 = 50;
		double number4 = number3;   // implicit casting done by JVM
		
		
	}
	
	void getData2() {
		long number5 = 10;
		int number6 = (int)number5; // explicit casting done by Developer
		
		double number7 = 30;
		float number8 = (float) number7;  // explicit casting done by developer
	}
	
	void getData3() {
		String info1 = "Hello";
		Object info2 = info1;      //UpCasting done by JVM
		
		Object info3 = "Hi";
		String info4 = (String)info3;  // DownCasting done by Developer. Object class is the super class of all the classes in Java
		                               // if want to change from super class to child class, must do downcasting by developer.
	}
	
	void getData4() {
		int number9 = 1000;
		Integer refInteger = number9;     // Autoboxing done by JVM
		
	}
	
	void getData5() {
		Double refdouble = 500.123;
		double number10 = refdouble; // Unboxing done by JVM
	}
	
	
}



public class Example27 {

	public static void main(String[] args) {
		

	}

}
