package day8;

 

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

 

class BufferReaderDemo{
    
    void getName() throws IOException {
        BufferedReader refBufferedReader = new BufferedReader( new InputStreamReader(System.in));
        System.out.println("Enter your name : ");
        
        String name1 = refBufferedReader.readLine();
        System.out.println(name1);
        
        System.out.println("Enter a character : ");
        
        char charData = (char)refBufferedReader.readLine().charAt(3);
        System.out.println(charData);
        
    }
    
}

 

public class Example28 {

 

    public static void main(String[] args) throws IOException {
        
        new BufferReaderDemo().getName();
        
    }

 

}