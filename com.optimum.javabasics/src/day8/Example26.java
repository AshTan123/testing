package day8;

// how to take user input from Eclipse IDE : Command Line Argument

public class Example26 {

	public static void main(String[] args) {
		
		String data = args[2];
		//int condition = Integer.parseInt(data);//
		//int anyname = 0;
		//System.out.println(data[1]);
		//int index = get(data, 1);
		
		
		switch (data) {
		case "Paris":
			System.out.println("Paris");
			break;
			
		case "Tokyo":
			System.out.println("Tokyo");
			break;
			
		case "Singapore":
			System.out.println("Singapore");
			break;

		default:
			System.out.println("Data not found");
			break;
		}
		

	} // end of main()

} // end of Example26
