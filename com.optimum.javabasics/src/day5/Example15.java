package day5;

class Driver{
	
	private static Driver refDriver = null;
	
	private Driver(){
		System.out.println("calling Driver constructor...");
		
	}
	
	public static Driver getMethod()
	{
		/*Driver refDriver = new Driver();*/
		if (refDriver == null) {
			
			refDriver = new Driver();
			
		}
		
		return refDriver;
		
	} // end of getMethod();
	
	
} // end of Driver


public class Example15 {

	public static void main(String[] args) {
		
		Driver.getMethod();
		Driver.getMethod();
		Driver.getMethod();
		Driver.getMethod();
		Driver.getMethod();
		

	}

}
