package day5;

abstract class KIOSK{
	
	void payBill1() {        // concrete method which has body{ where we can write our statement/code
		//method with a body part of the code
	}
	
	abstract void payBill2(); // abstract method without any implementation
} // end of Kiosk Class

class StarHub extends KIOSK{

	@Override
	void payBill2() {
		
		System.out.println("pay bill...");
		
	}
	
} // end of StarHub class


class Syntell extends KIOSK{

	@Override
	void payBill2() {
		
		System.out.println("pay bill...");
		
	}
	
}

public class Example17 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
