package day5;


abstract class Microsoft {
	
	abstract void getMicrosoftDriver();
	
} // end of Microsoft

abstract class Amazon{
	
	abstract void getAmazonDriver();
	
}// end of Amazon

abstract class Google {
	
	abstract void getGoogleDriver();
	
}// end of google

class OptimumApplication {
	
	// override all the abstract method from Microsoft, Amazon and Google and Implement here
	Microsoft refMicrosoft = new Microsoft() {
	
		@Override
		public void getMicrosoftDriver() {
			
			System.out.println("Getting Microsoft Driver.......");
		}
	};
	
	Google refGoogle = new Google() {
		@Override
		public void getGoogleDriver() {
			
			System.out.println("Getting Google Driver.......");
		}
	};
	
	Amazon refAmazon = new Amazon() {

		@Override
		void getAmazonDriver() {
			
			System.out.println("Getting Amazon Driver.......");
			
		}
	};
	
}

public class Example18 {

	public static void main(String[] args) {
		
		// call optimum and run application
		OptimumApplication op = new OptimumApplication();
		op.refAmazon.getAmazonDriver();
		op.refGoogle.getGoogleDriver();
		op.refMicrosoft.getMicrosoftDriver();
		
		// note: we cannot create object of abstract class//
		

	}

}
