package day15;

class ThreadDemo1 extends Thread{
	
	@Override
	public void run() {
		System.out.println("ThreadDemo1 ==> Hello from run method..");
	}
}

class ThreadDemo2 implements Runnable{
	
	@Override
	public void run() {
		System.out.println("ThreadDemo2 ==> Hello from run method...");
	}
}

public class Example56 {

	public static void main(String[] args) {
	
		ThreadDemo1 refThreadDemo1 = new ThreadDemo1();
		Thread refThread = new Thread(refThreadDemo1);
		refThread.start();
		
		ThreadDemo2 refThreadDemo2 = new ThreadDemo2();
		Thread refThread2 = new Thread(refThreadDemo2);
		refThread2.start();
		
		Runnable refRunnable = new Runnable() {
			@Override
			public void run() {
				System.out.println("Runnable  ==> Hello from run method...");
			}
		};
		
		Thread refThread3 = new Thread(refRunnable);
		refThread3.start();
		

	}

}
