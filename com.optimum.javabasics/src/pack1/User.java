package pack1;

public class User {

	public void getData1() {
		
		System.out.println("I am in public...");
		
	}
	
	private static void getData2() {
		
		System.out.println("I am in private...");
	}
	
	protected static void getData3() {        // what if we don't write static
		
		System.out.println("I am in default...");
	}
	
	void getData4() {
		
		System.out.println("I am in default...");
	}
	
}
