package day10;

// String class

public class Example36 {
	
	
	
	public static void main(String[] args) {
		
		String ref1 = "data1";
		String ref2 = "data1";
		String ref3 = new String("data1");
		String ref4 = new String("data1");
		
		Person refPerson1 = new Person("data1");
		Person refPerson2 = new Person("data2");

		// to check ref1 and ref2 is same, using == operator
		if(ref1 == ref2) {
			System.out.println("same");
		}else {
			System.out.println("not same");
		}
		
		// to check ref1 and ref2 is same, using .equals() method
		if(ref1.equals(ref2)) {
			System.out.println("same");
		}else {
			System.out.println("not same");
		}
		
		// to check ref2 and ref3 is same, using == operator
		if(ref2 == ref3) {
			System.out.println("same");
		}else {
			System.out.println("not same");
		}
		
		// to check ref2 and ref3 is same, using .equals() method
		if(ref2.equals(ref3)) {
			System.out.println("same");
		}else {
			System.out.println("not same");
		}
		
		// to check ref3 and ref4 is same, using == operator
		if(ref3 == ref4) {             // both are pointing to different memory address or location
			System.out.println("same");
		}else {
			System.out.println("not same"); // thats why we are getting not same
		}
		
		// check ref3 and ref4 is same using .equals
		if(ref3.equals(ref4)) {  // both are pointing to same memory address in the String Constant Pool in Heap Memory (JVM)
			System.out.println("ref3 and ref4 ==> same"); // Thats why we are getting same
		}else {
			System.out.println("not same");
		}
		
		// to check refPerson1 and refPerson2 is same, using .equals()
		if(refPerson1.equals(refPerson2))
		{
			System.out.println("refPerson1 and refPerson2 ==> same");
		}else {
			System.out.println("refPerson1 and refPerson2 ==> Not same");
		}
		
		System.out.println(refPerson1.hashCode());
		System.out.println(refPerson2.hashCode());
		
		System.out.println(refPerson1.equals(refPerson2));
	}

}
