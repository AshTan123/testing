package day10;

import java.util.Scanner;

class OuterClassDemo{
	
	public void getInnerClassMethod() {
		refInnerClassDemo.getDetailsInnerClassDemo();
	}
	
	// non-static inner class
	static class InnerClassDemo{ // nested class or inner class used for security purpose
		
		 void getDetailsInnerClassDemo() {     
			System.out.println("Enter your password :  ");
			Scanner refScanner = new Scanner(System.in);
			String password = refScanner.next();
			System.out.println(password);
			
		}
		
		
	} // end of InnerClassDemo class
	
	InnerClassDemo refInnerClassDemo = new InnerClassDemo();
	
} // end of OuterClassdemo class

public class Example33 {

	public static void main(String[] args) {
		
		OuterClassDemo refOuterClassDemo = new OuterClassDemo();
		refOuterClassDemo.getInnerClassMethod();
		
		/*refOuterClassDemo.refInnerClassDemo.getDetailsInnerClassDemo();*/ // approach 1
		
		/*OuterClassDemo refOuterClassDemo = new OuterClassDemo();
		OuterClassDemo.InnerClassDemo refInnerClassDemo = refOuterClassDemo.new InnerClassDemo();
		refInnerClassDemo.getDetailsInnerClassDemo();*/ //approach 2//
		
		// none of the approach will work if the inner class is private
		
		
		
		
		
		

	}

}
