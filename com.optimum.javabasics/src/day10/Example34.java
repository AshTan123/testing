package day10;

class OuterA{
	
	void methodOfOuterA() {          // method local inner class
		
		int number = 10;
		
		class InnerB{
			
			int number = 15;
			
			void methodOfInnerB() {
				System.out.println(number);
			}  // end of methodOfInnerB()
		} // end of InnerB class
		
		InnerB inner = new InnerB();
		inner.methodOfInnerB();
		System.out.println(number);
		
	} // end of methodOfOuterA()
	
	
} // end of OuterA class

public class Example34 {

	public static void main(String[] args) {
		
		OuterA outer = new OuterA();
		outer.methodOfOuterA();
		
	
	}

}
