package day3;

public class University {
	private int useruniversityID;
	private String universityName;
	private String universityLocation;
	
	
	public int getUseruniversityID() {
		return useruniversityID;
	}
	public void setUseruniversityID(int useruniversityID) {
		this.useruniversityID = useruniversityID;
	}
	public String getUniversityName() {
		return universityName;
	}
	public void setUniversityName(String universityName) {
		this.universityName = universityName;
	}
	public String getUniversityLocation() {
		return universityLocation;
	}
	public void setUniversityLocation(String universityLocation) {
		this.universityLocation = universityLocation;
	}
	

}
