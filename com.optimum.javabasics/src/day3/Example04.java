package day3;

// concept of static and non-static keyword
// static ==> no need to create any object, we can access static variable or method directly by providing the class name
// ==> ClassName.variablename or ClassName.methodName()

// non-static ==> we need to create object of that class to access non-static variable or method


// data member == variable + method

class Customer{
	
	static int customerID = 100;
	String customerName = "Asher";
	
	void getDetails1() // non static
	{
		// can we access customerID here? static data member
		System.out.println(customerID); // no error
		
		// can we access customerName here?
		System.out.println(customerName); // no error
		
	}// end of getDetails()
	
	
	static void getDetails2() { // static
		
		// can we access customerID here? static data member
		System.out.println(customerID); // no error
				
		// can we access customerName here?
		// System.out.println(customerName); // no error
		
		// to avoid the error we need to create object of Customer class
		Customer refCustomer = new Customer(); // we have create object of customer class 
		System.out.println(refCustomer.customerName);
		
		
	}// end of getDetails2()
	
	
} // end of Customer


public class Example04 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// call getDetails1() non static method
		Customer c = new Customer();
		c.getDetails1();
		
		// call getDetails2() static method
		Customer.getDetails2();
		
		//or
		
		c.getDetails2(); // static method

	}

}
