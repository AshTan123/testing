package day3;

// method overloading example1

public class Example07 {

	public static void main(String[] args) {
		main("hello from line 8"); // its calls line 13 
	} // end of main()
	
	public static void main(String data) { // data is going to hold the value
		 // print hello from line 8
		main(10);
		System.out.println(data);
	} // end of main()
	
	public static void main(int number) {  // number holds the value 10
		
		System.out.println(main("hello from line 20", 500)); // call line 25 and print the value
		System.out.println(number);   // print 10
		
	} // end of main()
	
	public static String main(String data, int number) {
		return data + " " +number;

	} // end of main
	
	

} // end of Example07
