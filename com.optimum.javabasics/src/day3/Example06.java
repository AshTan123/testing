package day3;

// Encapsulation Demo

public class Example06 {

	public static void main(String[] args) {
		University refUniversity = new University();
		refUniversity.setUseruniversityID(1000);
		refUniversity.setUniversityName("XYZ");
		refUniversity.setUniversityLocation("ABC");
		
		System.out.println(refUniversity.getUseruniversityID());

	}

}
