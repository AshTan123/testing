package day3;

// method overloading - example 2

class Mobile{
	
	private void getFeatures(boolean wifi) { // true or false
		System.out.println(wifi);
	}
	
	public String getFeatures(String brandName) {
		return brandName;
	}
	
	String getFeatures(int mobileCode, String mobileLocation) {
		return mobileCode + " " +mobileLocation;
	}
	
	void getFeature(long mobileDiscountOfferPrice, float mobilePrice) {
		System.out.println(mobileDiscountOfferPrice + " " + mobilePrice);
	}
	
	public void getFeature(boolean h)
	{
		getFeatures(h);
	}
}
public class Example08 {

	public static void main(String[] args) {
		Mobile ashmobile = new Mobile();
		System.out.println(ashmobile.getFeatures("Samsung"));
		System.out.println(ashmobile.getFeatures(12345, "Sentosa"));
		ashmobile.getFeature(67L, 89F);
		ashmobile.getFeature(true);

	}

}
