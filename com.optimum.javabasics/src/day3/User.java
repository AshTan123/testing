package day3;

// concept of local and global variable

class Userla{
	
	int userID = 1000;   // global variable 
	String userPassword = "user123"; // global variable
	
	void getDetails(int userID, String password) { // local variable ==> 50 and newuser123
		
		
	} // end of showDetails()
	
	void showDetails() {
		System.out.println(userID +" " +userPassword); // we are calling global variable
	}
	
	
}
public class User{

	public static void main(String[] args) {
		Userla refUser = new Userla();
		refUser.getDetails(50,  "newuser123");
		refUser.showDetails();

	}

}
