package day12;

public class ExceptionDemo02 {

	public static void main(String[] args) {
		
		int number [] = { 10, 20, 30};
		number[10] = 500; // 
		
		System.out.println(number);
		
		// Arithmetic 
		// Null Pointer
		// ArrayIndexOutOfBounds
		
		// how we can handle multiple exception??
		
		// one try
		// multiple catch block
		
	}

}
