package day12;

import java.io.*;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.lang.Math;

class Algorithm extends Exception{
	
	int sum;
	int number;
	
	void getUserInput(){
		
		try {
			
			Scanner scRef = new Scanner(System.in);
		    System.out.printf("Enter a number: ");
		    number = scRef.nextInt();
		    
		    checkUserInput(number);
		    
			for(int i = 1; i<=number; i++) {
				
				sum += Math.pow(i, i);
			}
			
			
			System.out.println("The sum of the series is " +sum);
		    
			
			
		}catch(Exception e) {
			
			System.out.println("Invalid input.");
		}
		
	}
	
	void checkUserInput(int number) throws Algorithm {
		
		  if(number < 0)
		  {
			    printDetails();
		    	Algorithm refAlgo = new Algorithm();
		    	throw refAlgo ;
		  }
		  
		  else {
			  printDetails2();
			 
		  }
	}
	
	void printDetails() {
		System.out.println("The number is invalid because it is negative");
	}
	
	void printDetails2() {
		System.out.println("Valid input");
	}
}

public class ExampleSpecial {

	public static void main(String[] args){
		
		Algorithm refAlgorithm = new Algorithm();
		refAlgorithm.getUserInput();
	}

}
