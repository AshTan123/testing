package day12;

class MyClass2{
	
	static int getID() {
		int number = 10;
		try {
			// lets calculate and modify the number
			number = 12/3;	// 62
		} // end of try
		catch (Exception e) {
			number = number + 8;
			// can we modify the number here? 
			// can we give return type??
			return number;
		} // end of catch
		
		finally { // can we modify the number here? 
			      // can we give return type, if yes how?
			number = number +8;
			return number; // if we put return inside finally then we have error on line 23.
						   // but we want to reutn the number from finally. what is the solution?
		} // end of finally
	} // end of getID()
} // end of MyClass2 

public class ExceptionDemo05 {

	public static void main(String[] args) {
		
		MyClass2 ref = new MyClass2();
		System.out.println(ref.getID());
		

	}

}
