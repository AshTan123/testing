package day12;

// Throwable class
// Exception and Error
// Exception => Checked (compile time)and Unchecked(Runtime)
// Error ==> Stack Overflow .. 

// What is exception?
// JVM performs 
// stack trace

class Person{
	void showPerson() {
		System.out.println("hello");
	}
}

public class ExceptionDemo01 {
	public static void main(String[] args) {
		
		Person refPerson=null;
		try {
			System.out.println("Open File..");
			refPerson.showPerson();
		} catch (Exception e) {
			System.out.println("Need to initizlize Person class");
		}
		
		finally {
			System.out.println("Closing File..");
			// will execute in both the cases => if we have exception or not
		}
		
	}

}
