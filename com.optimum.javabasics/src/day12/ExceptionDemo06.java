package day12;

import java.io.IOException;

class Parent{
	void showData()  throws ArithmeticException{   // Unchecked Exceptions
		System.out.println("Parent showData()..");
	}
} // end of Parent

class Child extends Parent{
	@Override
	void showData() throws ArithmeticException{ //  throws IOException (checked exception) compile time error
		
	}
	
//	@Override
//	void showData() throws ArrayIndexOutOfBoundsException { //  Unchecked exception no compile time error
		
//	}
	
/*
 * @Override void showData() throws Exception { // compile time error. Why
 * because, Exception is a Parent class of // ArithmenticException }
 */
	
}

public class ExceptionDemo06 {

	public static void main(String[] args) {
		
		

	}

}
