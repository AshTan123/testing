package pack2;

import pack1.User;

public class UserApplication2 extends User {

	public static void main(String[] args) {
		
		User refUser = new User();
		refUser.getData1();  // public can be access to anywhere
		getData3();          // static protected method using inheritance, we can access outside the package
		// refUser.getData4();  default method can't be called outside the package
		
		getData2(); // compilation error, we can't access private method variable outside the class and packages
		// we can call a protected method only if it is static
		// static private cant call but static protected can call
	}

}
