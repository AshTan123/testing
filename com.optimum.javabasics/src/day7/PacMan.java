package day7;



class Superman{
		
		private static Superman superman = null;
		
		private Superman(){
			System.out.println("I am Superman.");
			
		}
		
		public static Superman getMethod()
		{
			superman = new Superman();
			
			if (superman == null) {
				
				superman = new Superman();
				
			}
			
			return superman;
			
		} 
		
		
} 


public class PacMan{

	public static void main(String[] args) {
		
		Superman.getMethod();
		Superman.getMethod();
		Superman.getMethod();
		Superman.getMethod();
		Superman.getMethod();
		

	}

}



