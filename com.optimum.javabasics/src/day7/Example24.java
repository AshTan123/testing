package day7;

public class Example24 {

	public static void main(String[] args) {
		
		// create ref of chat class
		Chat refChat = new Chat();
		refChat.setChatMsg("How are you.......");
		
		WhatsApp refWhatsApp = new WhatsApp();
		refWhatsApp.setRefChat(refChat);
		
		Mobile refMobile = new Mobile();
		refMobile.setRefWhatsApp(refWhatsApp);
		
		System.out.println(refMobile.getRefWhatsApp().getRefChat().getChatMsg());

	}

}
