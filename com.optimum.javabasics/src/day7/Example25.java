package day7;

public class Example25 {

	public static void main(String[] args) {
		
		University refUni = new University(new Department(new Student("Tommy")));
		System.out.println(refUni);
		
		Student s = new Student("Dickson");
		Department d = new Department(s);
		University u = new University(d);
		System.out.println(u.toString());


	}

}
