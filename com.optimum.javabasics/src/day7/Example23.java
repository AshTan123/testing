package day7;

final class FinalDemo{
	
}

class SubFinalDemo extends FinalDemo2{
	
}

// note: if we are writing a base class as a final class, the base class cannot be extended. Final class for example user access permission, keep the class restricted from yourself

/*class Mobile{
	
	private void application() {
		
	}
}*/

// can we override final method from Mobile? NO
// final--> access specifier --> Access specifiers define how the members (attributes and methods) of a class can be accessed
// private, public, static --> access modifier 

class SubMobile extends Mobile{
	
	/*@Override
	void application() {
		
	}*/
}

class Laptop{

	final int number = 10; // final variable cant be changed or modified
	
	void getNumber(int value) {
		
		number = value;
	}
	
}



public class Example23 {

	public static void main(String[] args) {
		
		

	}

}
