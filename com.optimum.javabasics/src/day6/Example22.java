package day6;

@FunctionalInterface
interface One{
	void displayMethod1();
	void displayMethod2();
	
}
abstract class OptimumCar1{
	
	final static int number = 10;
	
	void display1() {
		// method which has a body
		
	}
	
	abstract void display2();
	
} // end of Car

interface OptimumCar2{
	
	int number = 10; // final static 
	
	void display3(){
		// method which has body
	}
	
	default void display4() {
		display5();
		display6();
	}
	
	static void display5() {
		System.out.println("I am in display 5");
	}
	
	private static void display6() {
		System.out.println("I am in display 6");
	}
	
	abstract void display8();
	
} // end of Optimumcar2

interface Baby{
	
}

class MyClass extends OptimumCar1 implements OptimumCar2, Baby{

	void display9() {
		display4();
	}
	
	@Override
	public void display3() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void display8() {
		
		System.out.println("I am in display 8.");
		
	}

	@Override
	void display2() {
		System.out.println("I am in display 2");
		
	}
	// can we extends more than 1 abstract class? NO
	//can we implement more than 1 interface? YES
	// abstract class abstract method is not public, interface class abstract method is public
	
}

public class Example22 {

	public static void main(String[] args) {
		
		MyClass refMyClass = new MyClass();
		refMyClass.display9();
		refMyClass.display8();
		refMyClass.display2();

	}

}
