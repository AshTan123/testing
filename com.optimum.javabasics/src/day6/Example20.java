package day6;

abstract class Uniqlo{
	
	int quantity;
	String customerName;
	
	public Uniqlo(){
		
		quantity = 10;
		customerName = "Peter";
		
	}
	
	
	abstract void withAirSim();
	abstract void withoutAirsim();
	
}

class Shirt extends Uniqlo{

	@Override
	void withAirSim() {
		
		System.out.println("Comfortable....");
		this.quantity = 6;
		this.customerName = "SAM";
		
	}

	@Override
	void withoutAirsim() {
		
		System.out.println("Uncomfortable....");
	}
	
}


public class Example20 {

	public static void main(String[] args) {
		
		Uniqlo refu = new Shirt();
		refu.withAirSim();//
		System.out.println(refu.quantity);
		System.out.println(refu.customerName);
		

	}

}
