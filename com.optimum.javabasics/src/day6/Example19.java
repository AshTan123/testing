package day6;

abstract class vehicle{
	
	abstract void withDriver();
	abstract void driverless();
	
} // end of Vehicle class

abstract class Car extends vehicle{
	
	abstract void supportAI();
}

abstract class MyCar extends Car{
	
	
} //

class Car1 extends MyCar{

	@Override
	void supportAI() {
		
		System.out.println("Inside supportsAI...");
	}

	@Override
	void withDriver() {
		
		System.out.println("Inside withDriver....");
		
	}

	@Override
	void driverless() {
		
		
	}
	
}


public class Example19 {

	public static void main(String[] args) {
		
		Car1 c = new Car1();
		c.withDriver();
		

	}

}
