package day6;

class Base{
	
	int number = 100;
	
	Base(int number){  // default constructor
		System.out.println(number); // 100 or 10
	}
	
} // end of Base Class

class Derived extends Base{

	Derived(int number) {
		super(number);
		System.out.println(number); // 10
		
	}
	
} // end of derived class

class SubDerived extends Derived{

	SubDerived(int number, String data) {
		super(number); // pass the value to the parent constructor 
		System.out.println(this.number + " " +data); // 10 data-1
		
	}
	
} // end of sub Derived class

public class Example21 {

	public static void main(String[] args) {
		
		new SubDerived(10, "data-1");

	}

}
