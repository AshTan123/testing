package Day4;



class Employee{
	
	Employee()
	{
		getDetails();
	}
	
	void getDetails()
	{
		new Department(101, "HR");
	}
} // end of Employee


class Department{
	
	int departmentID;
	String departmenttype;
	
	Department(int departmentID, String departmentType){
		
		this.departmentID = departmentID;
		this.departmenttype = departmentType;
		showDetails();
		
	}
	
	void showDetails()
	{
		System.out.println(departmentID +" " +departmenttype);
	}
}



public class Example12 {
	


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Employee();
	}

}
