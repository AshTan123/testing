package Day4;

// concept ==> Constructor

class User{
	
	int number = 10;
	

	User(int number, String name){
		this(true, 29292929);
		System.out.println(number +" " +name);
	}
	
	User(boolean data, long digit)
	{
		System.out.println(data +" " +digit);
	}
	
	User(){     
		
		this(50, "test01");
		number = 10;
		System.out.println(number);
		
	}  // end of constructor //
	
	
	
}

public class Example10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*new User(); // calls ==>  line 9
		new User(50, "test1"); // calls line 15
		new User(true, 5555);  // calls line 19*/
		
		new User();
		/*User userRef= new User();
		userRef.User(10, "test");*/

	} // end of main

}
