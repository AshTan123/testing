package Day4;

// concept ==> Inheritance
// java support multilevel inheritance
// java supports multiple inheritance through interface

class Programming{ // base class/ super class / parent class
	
	void showProgramming() {
		System.out.println("I am in programming class");
	}
	
} // end of programming

class Java extends Programming{ // base class/ super class / parent class
	
	void showJava() {
		System.out.println("I am in Java class");
	}
	
} // end of java

class Kotlin extends Java{ // base class/ super class / parent class
	
	void showKotlin() {
		System.out.println("I am in Kotlin class");
	}
	
} // end of Kotlin

class KotlinSecurity extends Kotlin{
	
	void getDetails() {
		showProgramming();
		showJava();
		showKotlin();
	}
}

class MyClass extends KotlinSecurity{
	
	void getDetails()
	{
		System.out.println("Data from my class....");
	}
	
	void getMyClass() {
		this.getDetails();
		super.getDetails();        // which getDetails to call???
	}
}

public class Example13 {

	public static void main(String[] args) {
		
		new MyClass().getMyClass();                 // line 40

	}

}
