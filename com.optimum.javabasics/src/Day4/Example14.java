package Day4;

// concept ==> overriding, dynamic polymorpherism 

class Laptop{
	
	void getDetails1() {
		
		System.out.println("I am in Laptop ==> getDetails1() ");
	}
	
	static void getDetails2() {
		System.out.println("I am in Laptop ==> getDetails2()");
	}
	
	
}

class OmenHP extends Laptop{
	
	@Override
	void getDetails1() {
			
			System.out.println("Doggo");
		}
	
	//@Override
	static void getDetails2() {
		System.out.println("Catto");
	}
		
}



public class Example14 {

	public static void main(String[] args) {
		
		Laptop reflaptop = new OmenHP();
		reflaptop.getDetails1();
		reflaptop.getDetails2();
		
		/*OmenHP refOmenHp = (OmenHP) new Laptop(); // type casting
		refOmenHp.getDetails1();
		refOmenHp.getDetails2();*/

	}

}
