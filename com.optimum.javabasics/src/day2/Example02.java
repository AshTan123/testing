package day2;

class Employee{ // class name
	
	static int employeeID = 100; //property or attribute or variable
	String employeeAddress = "CBP";
	
	static void employeeDetails1() // static method ==> verb/action()(method)
	{
		System.out.println(employeeID);
		
	} // end of employee Details
	
	void employeeDetails2()
	{
		System.out.println(employeeAddress); // can access non static
		System.out.println(employeeID); // can access static
	}
}

public class Example02 {
	
	
	static String data1 = "hello-1";
	static String data2 = "hello-2";

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// what is static???
		System.out.println(data1);
		System.out.println(data2);
		
		Employee.employeeDetails1(); // call method employeeDetails() no object created
		
		// how to create an object? why to create object?
		// how to access employeeDetails2()
		Employee e = new Employee();
		e.employeeDetails2();  // now we can access non-static method of employee class
		
		
	}

}
