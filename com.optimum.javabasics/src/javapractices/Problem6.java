package javapractices;

public class Problem6 {

	public static void main(String[] args) {
		
		for(int i = 0; i<5; i++) {
			if(i==0) {
				System.out.printf("*");
			}
			else {
				for(int j = 1; j<=i+(1*i); j++) {
					System.out.print("*");
				}
			}
			System.out.println();
		}

	}

}
