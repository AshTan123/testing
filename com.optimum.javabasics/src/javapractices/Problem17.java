package javapractices;
import java.util.Scanner;



public class Problem17 {
	
	static int fact(int n)
	{
	    if (n <= 1) // base case
	        return 1;
	    else    
	        return n*fact(n-1);    
	}

	public static void main(String[] args) {
		
		Scanner scRef = new Scanner(System.in);
		System.out.println("Enter a number:");
		int number = scRef.nextInt();
		int finalAns = 1;
		String equation = "";
		
		// find factorial using for loop
		System.out.println("Find factorial using for loop");
		for(int i = number; i>=1; i--) {
			finalAns *= i;
			if(i == 1) {
				equation +=  i +" * 1 = " +finalAns;
				break;
				
			}
			equation += i +" * ";
		}
		
		System.out.println("Factorial of " +number +" = " +finalAns);
		System.out.println(equation);
		
		
		// find factorial using while loop
		System.out.println();
		System.out.println("Find factorial using while loop.");
		int j = number;
		finalAns = 1;
		equation = "";
		while(j >= 1) {
			finalAns *= j;
			if(j == 1) {
				equation +=  j +" * 1 = " +finalAns;
				break;
				
			}
			equation += j +" * ";
			j--;
		}
		
		
		System.out.println("Factorial of " +number +" = " +finalAns);
		System.out.println(equation);
		
		// find factorial using biginteger
		
		// find factorial using recursion
		System.out.println();
		System.out.println("Find factorial using recursion");
	    System.out.println("The factorial of " +number +" is " +fact(number));

	}

}
