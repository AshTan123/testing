package javapractices;
import java.util.Scanner;
import java.lang.Math;

class CheckArmstrong{
	
	int numberofdigits;
	int summation;
	String thevalue = "";
	
	public CheckArmstrong() {
		// default constructor
	}
	
	public CheckArmstrong(int numberofdigits) {
		this.numberofdigits = numberofdigits;
	}
	
	public void check() {
		
		Scanner scRef = new Scanner(System.in);
		
		for(int i = 0; i<numberofdigits; i++) {
			System.out.println("Enter a digit");
			int newnumber = scRef.nextInt();
			summation += Math.pow(newnumber, numberofdigits);
			thevalue += Integer.toString(newnumber);
	    }
		
		if(Integer.parseInt(thevalue) == summation) {
			System.out.println(thevalue +" is an Armstrong number");
		}else {
			System.out.println(thevalue +" is not an armstrong number");
		}
	}
}

public class Problem18 {

	public static void main(String[] args) {
		
		Scanner scRef = new Scanner(System.in);
		System.out.println("Enter the number of digits");
		int thenumber = scRef.nextInt();
		CheckArmstrong caRef = new CheckArmstrong(thenumber);
		caRef.check();
		

	}

}
