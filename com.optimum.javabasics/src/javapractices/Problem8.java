package javapractices;

public class Problem8 {

	public static void main(String[] args) {
		
		int counter = 8;
		
		for(int i = 0; i<5; i++) {
			if(i == 4) {
				System.out.println("*");
			}
			for(int j = counter; j>i; j--) {
				System.out.printf("*");
				
			}
			
			if(i == 3) {
				System.out.println();
			}else {
				System.out.println();
				System.out.println();
			}
			
			counter--;
		}

	}

}
