package javapractices;
import java.util.Scanner;

class Palindrome{
	
	int i = 0;
	String number;
	String palindrome1 = "";
	String palindrome2 = "";
	
	public Palindrome(){
		// default constructor
	}
	
	public Palindrome(String number) {
		this.number = number;
	}
	
	public void forloop() {
		
		for(int i = 0; i<number.length(); i++) {
			palindrome1 += number.charAt(i);
		}
		
		for(int j = number.length(); j>0; j--) {
			palindrome2 += number.charAt(j-1);
		}
		
		System.out.println(palindrome1 + " " +palindrome2);
		
		if(palindrome1.equals(palindrome2)) {
			System.out.println(number +" is a palindrome");
		}else {
			System.out.println(number +" is not a palindrome");
		}
	}
	
	public void whileloop() {
		palindrome1 = "";
		palindrome2 = "";
		
		while(i<number.length()) {
			palindrome1 += number.charAt(i);
			i++;
		}
		
		i = 0;
		int j = number.length();
		
		while(j>0) {
			palindrome2 += number.charAt(j-1);
			j--;
		}
		
		System.out.println(palindrome1 + " " +palindrome2);
		
		if(palindrome1.equals(palindrome2)) {
			System.out.println(number +" is a palindrome");
		}else {
			System.out.println(number +" is not a palindrome");
		}
		
	}
	
}

public class Problem19 {

	public static void main(String[] args) {
		// program to check if number is a Palindrome
		Scanner scRef = new Scanner(System.in);
		System.out.println("Enter a number");
		String number = scRef.nextLine();
		Palindrome paRef = new Palindrome(number);
		// for loop
		paRef.forloop();
		paRef.whileloop();
		scRef.close();

	}

}