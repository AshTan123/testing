package javapractices;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.io.IOException;
import java.io.InputStreamReader;

class Product{
	
	int ProductID;
	String ProductName;
	int Price;
	int Quantity;
	

	public Product() {
		// constructor
	}
	
	public Product(int ProductID, String ProductName, int Price, int Quantity) {
		
		this.ProductID = ProductID;
		this.ProductName = ProductName;
		this.Price = Price;
		this.Quantity = Quantity;
	}
	
	public int getPrice()
	{
		int total = Price * Quantity;
		return total;
		
	}
	
	
	public void display() {
		
//		String PrintDetails = String.format(ProductID +"%.f" +ProductName +"%s" +Price +"%.f" +Quantity);
		System.out.format("%-15d %-20s  %-15d %-12d", ProductID, ProductName, Price, Quantity);
		System.out.println();

	}
	
}

public class Problem15 {

	public static void main(String[] args) throws IOException {
		
		int ID;
		String ProductName;
		int Price;
		int Quantity;
		int counter = 0;
		int sum = 0;
		int discount = 0;
		int finalprice = 0;
		
		ArrayList<Product> Products = new ArrayList<Product>();
		
		while(true) {
		
			// create new BufferedReader
			BufferedReader refBufferReader = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("Enter product ID: ");
			// get user input for product ID
			ID = Integer.parseInt(refBufferReader.readLine());
			
			// get user input for product Name
			System.out.println("Product Name: ");
			ProductName = refBufferReader.readLine();
			
			// get user input for price
			System.out.println("Price in SGD:  ");
			Price = Integer.parseInt(refBufferReader.readLine());
			
			// get user input for quantity
			System.out.println("Quantity: ");
			Quantity = Integer.parseInt(refBufferReader.readLine());
			
			Products.add(new Product(ID, ProductName, Price, Quantity));
			
			
			System.out.println("Wish to Continue: (Y?N): ");
			String YN = refBufferReader.readLine();
			
			counter++;
			
			
			if(YN.equals("N") || YN.equals("NO") || YN.equals("No") || YN.equals("n")) {
				
				System.out.println(YN);
				break;
			}
				
		
	    }
		
		System.out.println("Your product list as follows:");
		System.out.println("Product ID      Product Name          Price           Quantity");
		
		
		for(Product p : Products) {
			p.display();
			sum += p.getPrice();
		}
		
		System.out.println();
		System.out.println();
		System.out.println();
		
		
		System.out.println("Total Price:      " +sum);
		System.out.println("Flat Discount     20%");
		discount = sum/5;
		finalprice = sum - discount;
		System.out.println("Discount Amount   " +discount );
		System.out.println("Amount to Pay     " +finalprice);
		
		
		
	}

}
