package javapractices;

import java.util.Scanner;

public class Problem12 {

	public static void main(String[] args) {
		int numberofelements;
		int temp = 0;
		int secondlargest = 0;
		int secondsmallest = 0;
		Scanner scRef = new Scanner(System.in);
		System.out.println("Enter the number of elements:");
		numberofelements = scRef.nextInt();
		System.out.println("Input array elements: ");
		int array[] = new int[numberofelements];
		
		for(int i = 0; i<numberofelements; i++) {
			// reading array elements from the user
			array[i] = scRef.nextInt();
		}
		
		// bubble sort
		for(int i = 0; i<array.length; i++) {
			for(int j = 0; j<array.length; j++) {
				if(array[i] < array[j]) {
					temp = array[i];
					array[i] = array[j];
					array[j] = temp;
					
				}else {
					continue;
				}
			}
		}
		
		secondlargest = array[array.length - 2];
		secondsmallest = array[1];
		
		System.out.println("The second smallest value is " +secondsmallest);
		System.out.println("The second largest value is " +secondlargest);
		
		scRef.close();

	}

}
