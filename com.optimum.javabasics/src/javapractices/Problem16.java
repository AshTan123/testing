package javapractices;
import java.util.Scanner;

public class Problem16 {
	
	static int f1 = 0;
	static int f2 = 0;
	static int temp = 0;

	public static void main(String[] args) {
		// generate series using for loop
		for(int i = 0; i<13; i++) {
			if(i == 0) {
				f1 = 0;
				System.out.printf("0" +" ");
			}
			else if(i == 1) {
				f2 = 1;
				System.out.printf("1"+" ");
			}
			else {
				System.out.print(f2+f1 + " ");
				temp = f2 + f1;
				f1 = f2;
				f2 = temp;
				
			}
		}
		
		System.out.println();
		
		// generate series using while loop
		
		int j = 0;
		
		while(j<13) {
			if(j == 0) {
				f1 = 0;
				System.out.printf("0" +" ");
			}
			else if(j == 1) {
				f2 = 1;
				System.out.printf("1"+" ");
			}
			else {
				System.out.print(f2+f1 + " ");
				temp = f2 + f1;
				f1 = f2;
				f2 = temp;
				
			}
			
			j++;
			
		}
		
		System.out.println();
		// given number
		Scanner scRef = new Scanner(System.in);
		System.out.println("Enter a given number");
		
		int number = scRef.nextInt();
		
		for(int i = 0; i<number; i++) {
			if(i == 0) {
				f1 = 0;
				System.out.printf("0" +" ");
			}
			else if(i == 1) {
				f2 = 1;
				System.out.printf("1"+" ");
			}
			else {
				System.out.print(f2+f1 + " ");
				temp = f2 + f1;
				f1 = f2;
				f2 = temp;
				
			}
		}
		
	}

}
