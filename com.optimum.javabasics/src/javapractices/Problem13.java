package javapractices;

import java.util.ArrayList;
import java.util.Scanner;

public class Problem13 {

	public static void main(String[] args) {
		int numberofelements;
		boolean countercheck = false;
		Scanner scRef = new Scanner(System.in);
		System.out.println("Enter the number of elements:");
		numberofelements = scRef.nextInt();
		System.out.println("Input array elements: ");
		int array[] = new int[numberofelements];
		
		for(int i = 0; i<numberofelements; i++) {
			// reading array elements from the user
			array[i] = scRef.nextInt();
		}
		
		
		// add valid elements in arraylist
		ArrayList<Integer>List = new ArrayList<Integer>();
		
		List.add(array[0]);
		
		for(int i = 1; i<array.length; i++) {
			for(int j = 0; j<List.size(); j++) {
				countercheck = false;
				if(array[i] == List.get(j)) {
					//System.out.println(array[i] +" " +List.get(j));
					countercheck = true;
					break;
				}else {
					continue;
				}
			}
			
			if(countercheck == false) {
				List.add(array[i]);
			}
		}
		
		/*for(int i = 0; i<List.size(); i++) {
			System.out.println(List.get(i) +" ");
		}*/
		
		int resultarray[] = new int[List.size()];
		
		for(int i = 0; i<List.size(); i++) {
			resultarray[i] = List.get(i);
			
		}
		
		System.out.println("The final length of the array is " +resultarray.length);

	}

}
