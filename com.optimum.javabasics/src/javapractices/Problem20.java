package javapractices;
import java.util.Scanner;

class Palindromee{
	
	String word;
	String palindrome1 = "";
	String palindrome2 = "";
	
	public Palindromee() {
		// default constructor
	}
	
	public Palindromee(String newword) {
		this.word = newword;
	}
	
	public void withoutReverse() {
		for(int i = 0; i<word.length(); i++) {
			palindrome1 += word.charAt(i);
		}
		
		for(int j = word.length(); j>0; j--) {
			palindrome2 += word.charAt(j-1);
		}
		
		//System.out.println(palindrome1 + " " +palindrome2);
		
		if(palindrome1.equals(palindrome2)) {
			System.out.println(word +" is a palindrome");
		}else {
			System.out.println(word +" is not a palindrome");
		}
	}
	
	public void usingReverse(){
		// conversion of string object to stringbuffer
		StringBuffer sbrRef = new StringBuffer(word);
		//System.out.println(sbrRef +" " +sbrRef.reverse());
		if(sbrRef.toString().equals(sbrRef.reverse().toString())){
			System.out.println(word +" is a palindrome");
		}else {
			System.out.println(word +" is not a palindrome");
		}
		
		
	}
}

public class Problem20 {

	public static void main(String[] args) {
		Scanner scRef = new Scanner(System.in);
		System.out.println("Enter a word");
		String newword = scRef.nextLine();
		Palindromee paRef = new Palindromee(newword);
		
		System.out.println("Reverse String method");
		paRef.usingReverse();
		System.out.println();
		System.out.println("Without Reverse String method");
		paRef.withoutReverse();
		scRef.close();
		

	}

}
