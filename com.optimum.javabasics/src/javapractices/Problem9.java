package javapractices;

import java.io.*;
import java.util.Scanner;

//determine if array contains a particular value

public class Problem9 {

	public static void main(String[] args) {
		
		boolean trueorfalse = false;
		
		int numberarray[] = {1, 3, 5, 7, 9, 11};
		
		System.out.println("Enter a Number: ");
		Scanner scRef = new Scanner(System.in);
		int number = scRef.nextInt();
		
		for(int i = 0; i<numberarray.length; i++) {
			
			if(numberarray[i] == number) {
				trueorfalse = true;
				break;
			}else {
				trueorfalse = false;
			}
		}
		
		if(trueorfalse == true) {
			System.out.println("The element is inside the array.");
		}else {
			System.out.println("The element is not in the array.");
		}
		

	}

}
