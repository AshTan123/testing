package javapractices;

import java.util.ArrayList;
import java.util.Scanner;
import java.io.*;

class User{
	
	// email, password, securitykey, balance, withdrawal, deposit
	private String email;
	private String password;
	private String securitykey;
	private int balance = 0;
	private int withdrawal;
	private int deposit;
	
	public User() {
		// default constructor
	}
	
	public User(String email, String password, String key) {
		this.email = email;
		this.password =  password;
		securitykey = key;
	}
	

	public String getEmail() {
		return email;
	}
	

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setSecuritykey(String securitykey) {
		this.securitykey = securitykey;
	}

	public String getPassword() {
		return password;
	}


	public String getSecuritykey() {
		return securitykey;
	}

	public void setWithdrawal(int withdrawal) {
		this.withdrawal = withdrawal;
	}
	
	public void setDeposit(int deposit) {
		this.deposit = deposit;
	}
	
	public void setBalance() {
		balance += deposit;
		balance -= withdrawal;
	}
	
	public int getBalance() {
		return balance;
	}
	
}


public class Problem14 {
	
	static ArrayList<User> UserList = new ArrayList<User>();
	
	static void login() {
		
		Scanner sc = new Scanner(System.in);
		
		int accountnumber = 0;
		
		
		do {
		boolean counter = true;
		System.out.println("Enter User ID: ");
		String userID = sc.nextLine();
		System.out.println("Password: ");
		String password = sc.nextLine();
		
		for(int i = 0; i<UserList.size(); i++) {
			
			if(UserList.get(i).getPassword().equals(password) && UserList.get(i).getEmail().equals(userID) ) {
				System.out.println("Login Successful!!!");
				counter = false;
				accountnumber = i;
				break;
			}else {
				System.out.println("Invalid userID or password");
			}
	
		}
		
		if(counter == false) {
			break;
		}
		
		
		}while(true);
		
		System.out.println("Type 1: Check Available Bank Balance");
		System.out.println("Type 2: Deposit Amount");
		System.out.println("Type 3: Withdraw Amount");
		System.out.println();
		boolean stop = false;
		
		do {
		System.out.println("Enter Your Choice: ");
		System.out.println();
		String thechoice = sc.next();
		
		switch(Integer.parseInt(thechoice)) {
		
		// check account balance
		case 1:
			System.out.println("Available Balance: " +UserList.get(accountnumber).getBalance());
			System.out.println("Wish to Continue? (y/n):");
			String yn = sc.next();
			//System.out.println(yn);
			if(yn.equals("n")) {
				System.out.println("Thank you for banking with us!!!");
				stop = true;
			}
			break;
		
		// deposit amount
		case 2:
			
			do {
			System.out.println("Enter Amount: ");
			String amount = sc.next();
			
			
			if(Integer.parseInt(amount) < 0) {
				System.out.println("Amount cant be negative");
			}else {
				System.out.println(amount +" dollar deposited successfully!!!");
				UserList.get(accountnumber).setDeposit(Integer.parseInt(amount));
				UserList.get(accountnumber).setWithdrawal(0);
				UserList.get(accountnumber).setBalance();
				break;
			}
			}while(true);
			
			System.out.println("Wish to Continue? (y/n):");
			String yn2 = sc.next();
			if(yn2.equals("n")) {
				System.out.println("Thank you for banking with us!!!");
				stop = true;
			}
			
			break;
			
		// withdraw amount
		case 3:
			System.out.println("Enter Amount");
			String withdrawamt = sc.next();
			
			if(UserList.get(accountnumber).getBalance() - Integer.parseInt(withdrawamt) < 0) {
				System.out.println("Sorry!!! Insufficient balance");
			}else {
				
				UserList.get(accountnumber).setWithdrawal(Integer.parseInt(withdrawamt));
				UserList.get(accountnumber).setDeposit(0);
				UserList.get(accountnumber).setBalance();
				System.out.println("Transaction Successful!!!");
				
			}
			
			System.out.println("Wish to Continue? (y/n):");
			String yn3 = sc.next();
			
			if(yn3.equals("n")) {
				System.out.println("Thank you for banking with us!!!");
				stop = true;
			}
			
			break;
			
		default:
			System.out.println("Choice not available!!");
			break;
			
		}
		
		//System.out.println(stop);
		
		if(stop == true){
			break;
		}

		
		}while(true);
	
		
	}
	
	static void register() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter email address: ");
		String emailaddress = sc.next();
		
		// check if the email address exist
		for(User u: UserList) {
			
			if(u.getEmail().equals(emailaddress)) {
			   System.out.println("email already exists");
			   break;
			}
			
		}
		
		
		System.out.println("Enter Password: ");
		String password = sc.next();
		
		do {
		System.out.println("Re-type Password: ");
		String repassword = sc.next();
		
		if(!password.equals(repassword))
		{
			System.out.println("Password dont match: ");
		}else {
			break;
		}
		
	    } while(true);
		
		System.out.println("What is your favourite colour?: ");
		String colour = sc.next();
		System.out.println(colour +" is your security key, incase if you forget your password.");
		
		// create new user object and store in arraylist
		User newuser = new User(emailaddress, password, colour);
		UserList.add(newuser);
		System.out.println("Registration Successful!!!");
		
	}
	
	static void forgetPassword() {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter email address: ");
		String emailaddress = sc.nextLine();
		boolean checkifexist = true;
		int theaccount = 0;
		
		// check if the email address exist
		for(int i = 0; i<UserList.size(); i++) {
			
			if(UserList.get(i).getEmail().equals(emailaddress)) {
			   theaccount = i;
			   break;
			}else {
				checkifexist = false;
			}
			
		}
		
		if(checkifexist == false) {
			System.out.println("No such account!!!");
			sc.close();
			return;
		}
		
		
		System.out.println("Enter Password: ");
		String password = sc.nextLine();
		
		do {
		System.out.println("Re-type Password: ");
		String repassword = sc.nextLine();
		
		if(!password.equals(repassword))
		{
			System.out.println("Password dont match: ");
		}else {
			break;
		}
		
	    } while(true);
		
		System.out.println("What is your favourite colour?: ");
		String colour = sc.nextLine();
		System.out.println(colour +" is your security key, incase if you forget your password.");
		UserList.get(theaccount).setPassword(password);
		UserList.get(theaccount).setSecuritykey(colour);
		System.out.println("Your password has been reset successfully.");
		
		
	}
	
	static void logout(){
		System.out.println("Logout Successfully.");
		System.exit(0);
	}
	
	static void printDetails(){
		
		// declare the variables
		int choice;

	
		// print out the details
		System.out.println();
		System.out.println();
		System.out.println("Create console based Java Application(ATM):");
		System.out.println();
		System.out.println("User home Page:");
		System.out.println("1. Register");
		System.out.println("2. Login");
		System.out.println("3. Forget password");
		System.out.println("4. Logout(exit)");
		
		// get user's input
		Scanner scRef = new Scanner(System.in);
		System.out.println("Enter your Choice: ");
		choice = scRef.nextInt();
		scRef.nextLine();
		//scRef.close();
		
		switch(choice) {
		
		case 1:
			Problem14.register();
			break;
			
		case 2:
			login();
			break;
			
		case 3:
			forgetPassword();
			break;
			
		case 4:
			logout();
			break;
			
		default:
			
			System.out.println("Invalid choice. Going back to main menu");
			break;
			
			
		}
		
		//scRef.close();
		
		
	}
	
		
	

	public static void main(String[] args){
		
		boolean BooRef = true;
		
		do {
		Problem14.printDetails();
		}while(BooRef = true);
	

	}

}