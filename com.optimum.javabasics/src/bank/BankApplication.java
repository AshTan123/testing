package bank;

import java.util.Scanner;

public class BankApplication {

	public static void main(String[] args) {
		
		Scanner scRef = new Scanner(System.in); // to take input from user
		
		System.out.println("enter your choice:   ");
		
		String choice = scRef.next();
		
		CentralBank refCentralBank = BusinessLogic.getDetails(choice);
		refCentralBank.getPolicy();

	}

}
