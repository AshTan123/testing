package bank;

public class BusinessLogic {

	public static CentralBank getDetails (String choice) {
		
		if(choice.contentEquals("bank1")) {
			return new bank1();
		} else if(choice.contentEquals("bank2")) {
			return new bank2();
		}
		else if(choice.contentEquals("nob")) {
			return new NotaBank();
		}
		
		return null;
	}
}
