package day9;

public class Example31 {

	public static void main(String[] args) {
		
		// creating single dimensional(1 D) array
		
		int productNumber[] = {10, 20, 30, 40, 50};
		
		for(int i : productNumber) {
			System.out.println(i);
		}
		
		// by declaring the array first and then allocating the memory for it by using new operator
		
		int productKey[];
		
		int productkey[] = new int[5];
		
		productkey[0] = 10;
		productkey[1] = 20;
		productkey[2] = 30;
		productkey[3] = 40;
		productkey[4] = 50;
		
		for(int i : productkey) {
			System.out.println("\nby using new operator");
			System.out.println(i);
		}
		

}
