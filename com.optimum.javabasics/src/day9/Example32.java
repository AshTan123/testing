package day9;

import java.util.Scanner;

public class Example32 {

	public static void main(String[] args) {
		
		System.out.println("How many products you want to insert to cart : ");
		Scanner refScanner = new Scanner(System.in);
		int number = refScanner.nextInt();
		// we have an array of Product class
		Product refProduct[] = new Product[number];
		
	
		
		for(int i=0; i< refProduct.length; i++) {
			
			System.out.println("Enter product ID:");
			int productID = refScanner.nextInt();
			
			System.out.println("Enter product Name");
			String productName = refScanner.next();
			
			refProduct[i] = new Product(productID,productName);
			
		}
		
		// fetch all records from the array
		for (Product product : refProduct)
		{
			System.out.println(product);
		}
		

	}

}
