package day9;

public class breakandcontinue {

	public static void main(String[] args) {
		
		for(int i=0; i<8; i++)
		{
			if(i==7)
			{
				System.out.println("The end");
				break;
			}
			
			else {
				System.out.println("Not yet end...");
				continue;
			}
		}

	}

}
