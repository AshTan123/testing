package day9;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Example29 {

	public static void main(String[] args) throws IOException {
		
		BufferedReader refBufferReader = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter a number:");
		
		//int number = refBufferReader.read();
		
		//int number = Integer.parseInt(refBufferReader.readLine());
		//System.out.println(number);
		
		double number1 = Double.parseDouble(refBufferReader.readLine());
		System.out.println(number1);
		
		// we can do for ==> float.parseFloat(), Long.parseLong() and so on for the wrapper classes.
		
		
	}

}
