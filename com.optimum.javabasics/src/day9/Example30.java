package day9;

import java.util.Scanner;

// do while loop


public class Example30 {

	public static void main(String[] args) {
		
		String userChoice = null;
		
		do {
		
		Scanner scannerRef = new Scanner(System.in);
		System.out.println("Enter your name : ");
		
		String userName = scannerRef.next();
		
		System.out.println("Wish to continue(yes/no) : ");
		
		userChoice = scannerRef.next();
		
	    }while(!userChoice.equals("no"));
		
		System.out.println("Thank you!");
		
	}

}
